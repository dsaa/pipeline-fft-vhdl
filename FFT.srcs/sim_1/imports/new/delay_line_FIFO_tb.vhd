----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.04.2017 20:15:50
-- Design Name: 
-- Module Name: delay_line_FIFO_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity delay_line_FIFO_tb is
--  Port ( );
end delay_line_FIFO_tb;

architecture Behavioral of delay_line_FIFO_tb is
    

    
    constant DELAY :natural := 4;
    constant DATA_WIDTH :natural := 32;
    
    component delay_line_FIFO is
    Generic(DATA_WIDTH: integer := DATA_WIDTH;
            DELAY: integer := DELAY);
    Port (  data_in: in signed(DATA_WIDTH-1 downto 0);
           data_out: out signed(DATA_WIDTH-1 downto 0);
           clk: in std_logic;
           rst: in std_logic);
    end component;
    
    signal clk: std_logic := '0';
    signal rst: std_logic := '1';
    signal data_in:  signed(DATA_WIDTH-1 downto 0);
    signal data_out:  signed(DATA_WIDTH-1 downto 0);
    
   begin
       clk <= not clk after 5 ns;
       rst <= '1', '0' after 20 ns;
       UUT: delay_line_FIFO Port Map(data_in => data_in,
                               data_out => data_out,
                               rst => rst,
                               clk => clk);
       
        process(clk)
        variable j : integer range 0 to DELAY := 0;
        begin
           if rising_edge(clk) then
               if rst = '0' then
                   if j /= 2*DELAY+4 then
                        data_in <= (to_signed(j,DATA_WIDTH));
                        j := j + 1;
                   else
                       data_in <= (others => '0');          
                   end if;
               end if;
           end if;
        end process;
       
end Behavioral;