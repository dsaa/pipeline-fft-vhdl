----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.09.2017 13:39:58
-- Design Name: 
-- Module Name: experiments2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.math_real.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity experiments2 is
    Generic(constant DATA_WIDTH : integer := 8;
            constant DELAY : integer := 32;
            constant COS_VALUE_WIDTH: integer := 17);
    Port ( clk : in STD_LOGIC;
           d_in : in STD_LOGIC_VECTOR (DATA_WIDTH-1 downto 0);
           d_out : out STD_LOGIC_VECTOR (COS_VALUE_WIDTH-1 downto 0);
           ren: in std_logic);
end experiments2;

architecture Behavioral of experiments2 is
type rom_type is array(0 to DELAY*2/4-1) of std_logic_vector(COS_VALUE_WIDTH-1 to 0); --����� �������� �� N/2 ������ � twister ���� ��������� �� exp(j2pi/2\N)
    
    function init(constant length: in integer; constant width: in integer) return rom_type is
        variable y: rom_type := (others => (others => '0'));
        variable x: real := 0.0;    
        variable k: integer := 0;
        constant N: integer := length/4; --����� ������ �������� ������� �������� ��� ��������������� ���� �������� sin � cos
        constant pi: real := math_pi; --pi*2**29
        variable temp1: integer;
        --variable temp2: integer;    --������?
    begin
        y(0) := (width-1 => '0', others => '1'); --cos(0) = 0.9999999
        for k in 1 to N-1 loop                                    
            x := 2.0*pi/real(length)*real(k);            
            temp1 := integer(round(cos(x)*2.0**(width-1)));--���� ��� ��������� �� ����, ������� width-1  --(integer(floor(1.0*real(2**(width+8)))) - integer(floor(x**2/4.0*real(2**(width+8)))) + integer(floor(x**4/24.0*real(2**(width+8)))) - integer(floor(x**6/720.0*real(2**(width+8)))) + integer(floor(x**8/40320.0*real(2**(width+8)))));           
            y(k) := std_logic_vector(to_signed(temp1,width));
        end loop;
    return y;
    end function;
    
    constant rom : rom_type := init(DELAY*2,COS_VALUE_WIDTH);
    
    function cos_val(length: in integer; width: in integer; m: in integer) return signed is
        variable y: signed(width-1 downto 0) := (others => '0');
        variable N: integer := length;
    begin
        if m < N/4 then
            y := signed(rom(m));
        elsif m >= N/4 and m < N/2 then
            y := -signed(rom(N/2-1-m));
        elsif m >= N/2 and m < 3*N/4 then
            y := -signed(rom(m-N/2));
        elsif m >= 3*N/4 and m < N then
            y := signed(rom(N-1-m));
        else
            report("Error")
            severity ERROR;
        end if;
    end function;
    
    function sin_val(length: in integer; width: in integer; m: in integer) return signed is
        variable y: signed(width-1 downto 0) := (others => '0');
        variable N: integer := length;
    begin
        if m < N/4 then
            y := signed(rom(N/4-1-m));
        elsif m >= N/4 and m < N/2 then
            y := signed(rom(m - N/4));
        elsif m >= N/2 and m < 3*N/4 then
            y := -signed(rom(3*N/4-1 - m));
        elsif m >= 3*N/4 and m < N then
            y := -signed(rom(m - 3*N/4));
        else
            report("Error")
            severity ERROR;
        end if;
    end function;
    
begin
process(clk)
begin
  if(clk'event and clk = '1') then
    if(ren = '1') then
       d_out <= rom(to_integer(unsigned(d_in)));
    end if;
  end if;
end process;

end Behavioral;
