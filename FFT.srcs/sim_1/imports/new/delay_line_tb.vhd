----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 31.03.2017 19:44:15
-- Design Name: 
-- Module Name: delay_line_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


entity delay_line_tb is
--  Port ( );
end delay_line_tb;

architecture Behavioral of delay_line_tb is
    component delay_line is
        Generic (constant DELAY : natural := 32;
           constant DATA_WIDTH: natural := 32); 
       Port (data_in :     in      std_logic_vector(DATA_WIDTH-1 downto 0);
             data_out:     out     std_logic_vector(DATA_WIDTH-1 downto 0);
             clk :         in      std_logic;
             rst :         in      std_logic);
    end component;
    
    constant DELAY :natural := 32;
    constant DATA_WIDTH :natural := 32;
    signal clk: std_logic := '0';
    signal rst: std_logic := '1';
    signal data_in:  std_logic_vector(DATA_WIDTH-1 downto 0);
    signal data_out:  std_logic_vector(DATA_WIDTH-1 downto 0);
    type delay_ram is array(DATA_WIDTH-1 downto 0) of std_logic_vector(DELAY-1 downto 0);
begin
    clk <= not clk after 5 ns;
    rst <= '1', '0' after 20 ns;
    UUT: delay_line Port Map(data_in => data_in,
                            data_out => data_out,
                            rst => rst,
                            clk => clk);
    
    process(clk)
    variable data: delay_ram := (others => (others => '1'));
    variable j : integer range 0 to DELAY := 0;
    begin
        if rising_edge(clk) then
        if rst = '0' then
            if j /= DELAY then
                for i in DATA_WIDTH-1 downto 0 loop
                    data_in(i) <= data(i)(j);
                end loop;
                j := j + 1;
            else
                data_in <= (others => '0');          
            end if;
        end if;
        end if;
    end process;
end Behavioral;
