----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.04.2017 22:33:26
-- Design Name: 
-- Module Name: FIFO_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FIFO_tb is
--  Port ( );
end FIFO_tb;

architecture Behavioral of FIFO_tb is
    component FIFO is
        Generic(DATA_WIDTH: integer := 32;
                DEPTH: integer := 128);
        Port (  data_in: in std_logic_vector(DATA_WIDTH-1 downto 0);
                data_out: out std_logic_vector(DATA_WIDTH-1 downto 0);
                clk: in std_logic;
                rst: in std_logic;
                rstout:out std_logic;
                wen: in std_logic;
                ren: in std_logic;
                is_full: out std_logic;
                is_empty: out std_logic);
    end component;
    
    signal data_in: std_logic_vector(31 downto 0);
    signal data_out: std_logic_vector(31 downto 0);
    signal clk: std_logic := '0';
    signal rst: std_logic;
    signal rstout: std_logic;
    signal wen: std_logic;
    signal ren: std_logic;
    signal is_full: std_logic;
    signal is_empty: std_logic;
    
begin

    UUT: FIFO Port Map(data_in => data_in,
                        data_out => data_out,
                        clk => clk,
                        rst => rst,
                        rstout => rstout,
                        wen => wen,
                        ren => ren,
                        is_full => is_full,
                        is_empty => is_empty);

    clk <= not clk after 5 ns;
    rst <= '1', '0' after 40 ns;
    
    process(clk)
    variable i: integer:=0;
    begin
        if rising_edge(clk) then
            if rst = '1' then
                wen <= '0';
                ren <= '0';        
            elsif i /= 128 then
                wen <= '1';
                data_in <= std_logic_vector(to_unsigned(i,32));                
                if i >= 61 then --=DELAY-3
                    if is_empty /= '1' then
                        ren <= '1';
                    end if;
                end if;
                i := i + 1;
            else
                wen <= '0';
                ren <= '0';
            end if;
            
        end if;
    end process;
    
end Behavioral;
