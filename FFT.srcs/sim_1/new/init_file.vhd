----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.09.2017 16:18:19
-- Design Name: 
-- Module Name: init_file - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use std.textio.all;
use ieee.math_real.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity init_file is
--  Port ( );
end init_file;

architecture Behavioral of init_file is
    constant FILE_NAME : string := "cos_values_length_";
    constant COS_VALUE_WIDTH : integer := 17;
    type arLengths is array(natural range <>) of integer;
    --����������� ������ ����� 2 � 4
    constant LENGTHS: arLengths(0 to 10) := (1,2,4,8,16,32,64,128,256,512,1024);
    --type ram_type is array(natural range <>) of signed(COS_VALUE_WIDTH-1 downto 0); 
    
    procedure print_to_file(file f_out : TEXT; value : in integer) is --������ �� ����� � ����� ������?
        variable l_out : line;
        variable slv_vec : std_logic_vector(COS_VALUE_WIDTH-1 downto 0);
    begin
        slv_vec := std_logic_vector(to_signed(value,COS_VALUE_WIDTH));
        write(l_out,to_bitvector(slv_vec));
--        for i in slv_vec'length-1 downto 0 loop    
--            write(l_out,to_bit(slv_vec(i)));
--        end loop;
        writeLine(f_out,l_out);
        
        
    end procedure;
    
    procedure init(constant length: in integer; constant width: in integer) is    
        --variable y: ram_type := (others => (others => '0'));
        file f_out : text open write_mode is FILE_NAME& integer'image(length)&".txt";
        variable l_out : line;
        variable x: real := 0.0;    
        variable k: integer := 0;
        constant N: integer := length/4; --����� ������ �������� ������� �������� ��� ��������������� ���� �������� sin � cos
        constant pi: real := math_pi; --pi*2**29
        variable temp1: integer;
        variable cos_value: std_logic_vector(width-1 downto 0); 
      
    begin
        cos_value := (width-1 => '0', others => '1'); --cos(0) = 0.9999999
        print_to_file(f_out,to_integer(signed(cos_value)));
        if length > 2 then                        
            --print_to_file(f_out,to_integer(signed(cos_value)));
            --write(l_out,string'image("\n"));        
            for k in 1 to N-1 loop                                    
                x := 2.0*pi/real(length)*real(k);            
                temp1 := integer(round(cos(x)*2.0**(width-1)));--���� ��� ��������� �� ����, ������� width-1  --(integer(floor(1.0*real(2**(width+8)))) - integer(floor(x**2/4.0*real(2**(width+8)))) + integer(floor(x**4/24.0*real(2**(width+8)))) - integer(floor(x**6/720.0*real(2**(width+8)))) + integer(floor(x**8/40320.0*real(2**(width+8)))));           
                cos_value := std_logic_vector(to_signed(temp1,width));         
                print_to_file(f_out,to_integer(signed(cos_value)));
            end loop;
            print_to_file(f_out,0);
        elsif length = 2 then
            print_to_file(f_out,0); --����� 16, ����� ����� �������� ����� 4 � ��� ����
        end if;
    --return y;
    
    end procedure;
    
    procedure init_f is
    
    begin
        for i in LENGTHS'range loop
            init(LENGTHS(i),COS_VALUE_WIDTH);
        end loop;
    end procedure;
    
    signal clk : std_logic;
begin
--    clk <= '0', '1' after 10 ns;
--    process(clk)                --����������� write �������� ������ ������ ��������, � ������� �� ������ ����������� assert report sevrity
--    variable l : line;
--    begin
--    assert false
--    report "NNNNNNOOOOO"
--    severity note;  
--       init_f;
--       write(l,string'("sssssssssssss")); -- ������ ���� ���������� ������ ���
--       writeLine(output,l);
--    assert false
--    report "YYYYEEEEESSSSS"
--    severity note;
--    end process;
init_f;

end Behavioral;
