----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.09.2017 13:07:34
-- Design Name: 
-- Module Name: FFT_block_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FFT_block_tb is
--  Port ( );
end FFT_block_tb;

architecture Behavioral of FFT_block_tb is
constant DATA_WIDTH : integer := 23;

component FFT_block is
    Generic(DATA_WIDTH: integer := DATA_WIDTH;      --��� ��� �� ����� ���������� ����� �� 1 ��� ������, � ������� ��� 24
            DELAY: integer := 16;            -- ����� �������� � twister 
            COS_VALUE_WIDTH: integer := 17;
            INIT_FILE : string := "G:\Temp_work\FFT\FFT.sim\sim_1\behav"
            );
    Port ( ar : in SIGNED (DATA_WIDTH-1 downto 0);
           ai : in SIGNED (DATA_WIDTH-1 downto 0);
           br : in SIGNED (DATA_WIDTH-1 downto 0);
           bi : in SIGNED (DATA_WIDTH-1 downto 0);
           yr : out SIGNED (DATA_WIDTH-1 downto 0);
           yi : out SIGNED (DATA_WIDTH-1 downto 0);
           zr : out SIGNED (DATA_WIDTH-1 downto 0);
           zi : out SIGNED (DATA_WIDTH-1 downto 0);
           clk : in std_logic;
           rst : in std_logic);
end component FFT_block;

signal ar: SIGNED (DATA_WIDTH-1 downto 0);
signal ai: SIGNED (DATA_WIDTH-1 downto 0);
signal br: SIGNED (DATA_WIDTH-1 downto 0);
signal bi: SIGNED (DATA_WIDTH-1 downto 0);
signal yr: SIGNED (DATA_WIDTH-1 downto 0);
signal yi: SIGNED (DATA_WIDTH-1 downto 0);
signal zr: SIGNED (DATA_WIDTH-1 downto 0);
signal zi: SIGNED (DATA_WIDTH-1 downto 0);
signal clk: std_logic := '0';
signal rst: std_logic;
signal rst_reg: std_logic;


begin
    
    UUT: FFT_block Port Map( ar => ar,
                            ai => ai,
                            br => br,
                            bi => bi,
                            yr => yr,
                            yi => yi,
                            zr => zr,
                            zi => zi,
                            clk => clk,
                            rst => rst_reg
                            );


    clk <= not clk after 10 ns;
    rst <= '1' , '0' after 10*5 ns - 5 ns;
    
    rst_reg <= rst when rising_edge(clk);
    
    process(clk)
    begin
        if rising_edge(clk) then
            if rst_reg = '1' then
                ar <= to_signed(1*2**10,DATA_WIDTH);
                br <= to_signed(1*2**10,DATA_WIDTH);
                ai <= to_signed(1*2**10,DATA_WIDTH);
                bi <= to_signed(1*2**10,DATA_WIDTH);
            else
                ar <= ar + to_signed(1*2**10,DATA_WIDTH);
                br <= br + to_signed(2*2**10,DATA_WIDTH);
                ai <= ai + to_signed(2*2**10,DATA_WIDTH);
                bi <= bi + to_signed(1*2**10,DATA_WIDTH);
                
                
            end if;
        end if;
    end process;


end Behavioral;
