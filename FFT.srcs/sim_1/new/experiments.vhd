----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.09.2017 21:36:34
-- Design Name: 
-- Module Name: experiments - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity experiments is
--  Port ( );
end experiments;

architecture Behavioral of experiments is
constant DELAY : integer := 8;
constant DATA_WIDTH : integer := 8;
signal a : integer range -3 to 2 := -2;
signal b : std_logic_vector(2 downto 0);
signal d_in : std_logic_vector(DATA_WIDTH+2-1 downto 0);
signal d_out: std_logic_vector(DATA_WIDTH+2-1 downto 0);
signal clk: std_logic := '0';
signal rst : std_logic := '0';
component delay_line_SRL is
    Generic (constant DELAY : natural := DELAY;
            constant DATA_WIDTH: natural := DATA_WIDTH+2); 
    Port (data_in :     in      std_logic_vector(DATA_WIDTH-1 downto 0);
          data_out:     out     std_logic_vector(DATA_WIDTH-1 downto 0);
          clk :         in      std_logic;
          rst :         in      std_logic);
end component delay_line_SRL;

begin
clk <= not clk after 10 ns;
rst <= '1', '0' after 50 ns;
    delay_line: delay_line_SRL Port Map (clk => clk,
                                        rst => rst,
                                        data_in => d_in,
                                        data_out => d_out);

d_in <= (others => '0'), (others => '1') after 100 ns, (0 => '1', 2 => '1', others => '0');
end Behavioral;
