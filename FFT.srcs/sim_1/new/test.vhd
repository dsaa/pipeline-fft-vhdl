----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.04.2017 12:03:33
-- Design Name: 
-- Module Name: test - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.math_real.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity test is
  Port (data_out: out std_logic_vector(18-1  downto 0) );
end test;

architecture Behavioral of test is
    signal clk: std_logic := '0';

    
    type rom_type is array(0 to 31) of std_logic_vector(18-1 downto 0);
    --type ram_t is array(natural range <>) of std_logic;
    --signal t : ram_t(0 to 4) := (others => '0');
    
    function init(length: in integer; width: in integer) return rom_type is
        variable y: rom_type := (others => (others => '0'));
        variable x: real := 0.0;    
        variable k: integer := 0;
        variable N: integer := length/4; --����� ������ �������� ������� ��������
        constant pi: real := math_pi; --pi*2**29
        variable temp1: integer;
        variable temp2: integer;    --������?
    begin
        y(0) := (18-1 => '0', others => '1'); --cos(0) = 0.9999999
        for k in 1 to N-1 loop                                    
            x := 2*pi/real(length)*real(k);            
            temp1 := integer(round(cos(x)*2.0**(width-1)));--���� ��� ��������� �� ����, ������� width-1  --(integer(floor(1.0*real(2**(width+8)))) - integer(floor(x**2/4.0*real(2**(width+8)))) + integer(floor(x**4/24.0*real(2**(width+8)))) - integer(floor(x**6/720.0*real(2**(width+8)))) + integer(floor(x**8/40320.0*real(2**(width+8)))));           
            y(k) := std_logic_vector(to_signed(temp1,width));
        end loop;
    return y;
    end function;
    
    signal rom: rom_type := init(32,18);
    
begin
    clk <= not clk after 10 ns;
    
    process(clk)
    --variable int : integer range 0 to 2**31-1 := 33;
    variable count : integer := 0;
    begin
    if rising_edge(clk) then
        if(count = 18) then
        else
            data_out <= rom(count) ;
            count := count + 1;
        end if;
    end if;
        
    end process;

end Behavioral;
