----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01.09.2017 17:58:24
-- Design Name: 
-- Module Name: cmult_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cmult_tb is
--  Port ( );
end cmult_tb;

architecture Behavioral of cmult_tb is

    constant AWIDTH: natural := 24;
    constant BWIDTH: natural := 17;

  component cmult is
  generic (AWIDTH : natural := AWIDTH; -- size of 1st input of multiplier
           BWIDTH : natural := BWIDTH  -- size of 2nd input of multiplier
          );
  port (clk    : in  std_logic;
        ar : in  std_logic_vector(AWIDTH-1 downto 0); -- 1st input's real part
        ai : in  std_logic_vector(AWIDTH-1 downto 0); -- 1st input's imaginary part
        br : in  std_logic_vector(BWIDTH-1 downto 0); -- 2nd input's real part
        bi : in  std_logic_vector(BWIDTH-1 downto 0); -- 2nd input's imaginary part
        pr : out std_logic_vector(AWIDTH+BWIDTH downto 0);  -- real part of output
        pi : out std_logic_vector(AWIDTH+BWIDTH downto 0)); -- imaginary part of output
    end component cmult;
    
    signal ar : std_logic_vector(AWIDTH-1 downto 0) := (others => '0');
    signal ai : std_logic_vector(AWIDTH-1 downto 0) := (others => '0');
    signal br : std_logic_vector(BWIDTH-1 downto 0) := (others => '0');
    signal bi : std_logic_vector(BWIDTH-1 downto 0) := (others => '0');
    signal pr : std_logic_vector(AWIDTH+BWIDTH downto 0) := (others => '0');
    signal pi : std_logic_vector(AWIDTH+BWIDTH downto 0) := (others => '0');
    
    signal clk : std_logic := '0';

begin

    clk <= not clk after 5 ns;
    
    ar <= std_logic_vector(to_signed(4,ar'length));
    ai <= std_logic_vector(to_signed(2,ai'length));
    br <= std_logic_vector(to_signed(-1,br'length));
    bi <= std_logic_vector(to_signed(5,bi'length));
    
    
    UUT: cmult Port Map(ar => ar,
                        ai => ai,
                        br => br,
                        bi => bi,
                        pr => pr,
                        pi => pi,
                        clk => clk);
                        
    

end Behavioral;
