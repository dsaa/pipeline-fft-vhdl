----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.09.2017 20:05:00
-- Design Name: 
-- Module Name: cos_val_rom_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.cos_ram_pkg.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cos_val_rom_tb is
--  Port ( );
end cos_val_rom_tb;

architecture Behavioral of cos_val_rom_tb is

constant COS_VALUE_WIDTH : integer := 17;
constant DELAY : integer := 16;
constant LENGTH : integer := 2*DELAY;

component cos_values is
generic (
    COS_VALUE_WIDTH : integer := COS_VALUE_WIDTH;                      -- Specify RAM data width
    LENGTH : integer := LENGTH;                    -- Specify RAM depth (number of entries)
    RAM_PERFORMANCE : string := "LOW_LATENCY";      -- Select "HIGH_PERFORMANCE" or "LOW_LATENCY" 
    INIT_FILE : string := "G:\Temp_work\FFT\FFT.sim\sim_1\behav"; --file path or blank 
    FILE_NAME_PATTERN: string := "\cos_values_length_"
    );

port (
        addra : in std_logic_vector((clogb2(LENGTH/4)+1-1) downto 0);     -- Write address bus, width determined from RAM_DEPTH
        addrb : in std_logic_vector((clogb2(LENGTH/4)+1-1) downto 0);     -- Read address bus, width determined from RAM_DEPTH
        --dina  : in std_logic_vector(COS_VALUE_WIDTH-1 downto 0);		  -- RAM input data
        --dinb  : in std_logic_vector(COS_VALUE_WIDTH-1 downto 0);		  -- RAM input data
        clka  : in std_logic;                       			  -- Clock
        --wea   : in std_logic;                       			  -- Write enable
        --web   : in std_logic;                       			  -- Write enable
        ena   : in std_logic;                       			  -- RAM Enable, for additional power savings, disable port when not in use
        enb   : in std_logic;                       			  -- RAM Enable, for additional power savings, disable port when not in use
        rsta  : in std_logic;                       			  -- Output reset (does not affect memory contents)
        rstb  : in std_logic;                       			  -- Output reset (does not affect memory contents)
        regcea: in std_logic;                       			  -- Output register enable
        regceb: in std_logic;                       			  -- Output register enable
        douta : out std_logic_vector(COS_VALUE_WIDTH-1 downto 0);   			  -- RAM output data
        doutb : out std_logic_vector(COS_VALUE_WIDTH-1 downto 0)   			  -- RAM output data
    );
end component cos_values;


    procedure get_cos_value(signal m: in integer;signal addra : out std_logic_vector)  is        
        constant N: integer := 2*DELAY; --���� ������ cos ����� LENGTH = DELAY*2
       
    begin
        
        if m < N/4+1 then
            addra <= std_logic_vector(to_unsigned(m, addra'length));           
        elsif m >= N/4+1 and m < N/2+1 then
            addra <= std_logic_vector(to_unsigned(N/2-m, addra'length));            
         elsif m >= N/2+1 and m < 3*N/4+1 then
            addra <= std_logic_vector(to_unsigned(m-N/2, addra'length));            
        elsif m >= 3*N/4+1 and m < N then
            addra <= std_logic_vector(to_unsigned(N-m, addra'length));                
        else
            report("Error cos value: "&integer'image(m))
            severity ERROR;
        end if;

    end procedure;
    
    
    procedure get_cos_sign(signal m_d: in integer; signal harm_value_a: in std_logic_vector; signal cos_value : out signed)  is        
    constant N: integer := 2*DELAY; --���� ������ cos ����� LENGTH = DELAY*2
       
    begin
        
        if m_d < N/4+1 then          
           cos_value <= (signed(harm_value_a));
        elsif m_d >= N/4+1 and m_d < N/2+1 then          
           cos_value <= -(signed(harm_value_a));   
        elsif m_d >= N/2+1 and m_d < 3*N/4+1 then           
           cos_value <= -(signed(harm_value_a)); 
        elsif m_d >= 3*N/4+1 and m_d < N then           
           cos_value <= (signed(harm_value_a));            
        else
           report("Error cos sign: "&integer'image(m_d))
           severity ERROR;
        end if;
    end procedure;
    
    
    procedure get_sin_value(signal m: in integer; signal addrb : out std_logic_vector)  is        
        constant N: integer := 2*DELAY; --���� ������ cos ����� LENGTH = DELAY*2
    begin
        if m < N/4+1 then
            addrb <= std_logic_vector(to_unsigned(N/4-m, addrb'length));           
        elsif m >= N/4+1 and m < N/2+1 then
            addrb <= std_logic_vector(to_unsigned(m - N/4, addrb'length));                  
        elsif m >= N/2+1 and m < 3*N/4+1 then
            addrb <= std_logic_vector(to_unsigned(3*N/4 - m, addrb'length));                      
        elsif m >= 3*N/4+1 and m < N then
            addrb <= std_logic_vector(to_unsigned(m - 3*N/4, addrb'length));                  
        else
            report("Error sin value: "&integer'image(m))
            severity ERROR;
        end if;
    end procedure;


    procedure get_sin_sign(signal m_d: in integer; signal harm_value_b: in std_logic_vector; signal sin_value : out signed)  is        
        constant N: integer := 2*DELAY; --���� ������ cos ����� LENGTH = DELAY*2
    begin
        if m_d < N/4+1 then            
            sin_value <= signed(harm_value_b);
        elsif m_d >= N/4+1 and m_d < N/2+1 then            
            sin_value <= signed(harm_value_b);            
        elsif m_d >= N/2+1 and m_d < 3*N/4+1 then           
            sin_value <= -signed(harm_value_b);            
        elsif m_d >= 3*N/4+1 and m_d < N then           
            sin_value <= -signed(harm_value_b);            
        else
            report("Error sin sign: "&integer'image(m_d))
            severity ERROR;
        end if;
    end procedure;


    signal addra : std_logic_vector((clogb2(LENGTH/4)+1-1) downto 0) := (others => 'U'); 
    signal addrb : std_logic_vector((clogb2(LENGTH/4)+1-1) downto 0) := (others => 'U');
    signal clk : std_logic := '0';
    signal douta : std_logic_vector((COS_VALUE_WIDTH-1) downto 0);
    signal doutb : std_logic_vector((COS_VALUE_WIDTH-1) downto 0);
    signal m , m_d, m_dd: integer := 0;
    signal cos_val, sin_val : signed(COS_VALUE_WIDTH-1 downto 0);
                             

begin
    UUT:cos_values Port Map(clka => clk,
                        addra => addra,
                        addrb => addrb,
                        ena => '1',
                        enb => '1',
                        rsta => '0',
                        rstb => '0',
                        regcea => '1',
                        regceb => '1',
                        douta => douta,
                        doutb => doutb);  
clk <= not clk after 10 ns;
-- process(clk)
-- variable a : integer := 0;
-- begin
--    if rising_edge(clk) then        
--        if a = LENGTH/4+1 then 
--        else
--            addra <= std_logic_vector(to_unsigned(a,clogb2(LENGTH/4)+1));
--            a := a + 1;
--        end if;
--    end if;
--end process;

process(clk)
begin
    if rising_edge(clk) then
        if m /= LENGTH then
            m <= m+1;            
        end if;
        if m_dd /= LENGTH then
            m_d <= m;
            m_dd <= m_d;            
        end if;
            
            get_sin_value(m, addrb);
            get_sin_sign(m_dd, doutb, sin_val);
            get_cos_value(m,  addra);
            get_cos_sign(m_dd, douta, cos_val);
            
        
    end if;
end process;

end Behavioral;
