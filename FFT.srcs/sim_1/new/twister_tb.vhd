----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07.04.2017 12:19:06
-- Design Name: 
-- Module Name: twister_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity twister_tb is
--  Port ( );
end twister_tb;

architecture Behavioral of twister_tb is

    constant DATA_WIDTH : integer := 7;
    constant DELAY : integer := 4;
    
    component twister is
    Generic (DATA_WIDTH : integer := DATA_WIDTH;
            DELAY : integer := DELAY);
    Port ( data_in_1 : in STD_LOGIC_VECTOR (DATA_WIDTH-1 downto 0);
           data_in_2 : in STD_LOGIC_VECTOR (DATA_WIDTH-1 downto 0);
           data_out_1 : out STD_LOGIC_VECTOR (DATA_WIDTH-1 downto 0);
           data_out_2 : out STD_LOGIC_VECTOR (DATA_WIDTH-1 downto 0);
           clk : in STD_LOGIC;
           rst : in STD_LOGIC
           );
    end component;
    
    signal clk : std_logic := '0';
    signal rst : std_logic := '0';
    signal data_in_1 : STD_LOGIC_VECTOR (DATA_WIDTH-1 downto 0);
    signal data_in_2 : STD_LOGIC_VECTOR (DATA_WIDTH-1 downto 0);
    signal data_out_1 : STD_LOGIC_VECTOR (DATA_WIDTH-1 downto 0);
    signal data_out_2 : STD_LOGIC_VECTOR (DATA_WIDTH-1 downto 0);
    
begin
    
    clk <= not clk after 5 ns;
    rst <= '1', '0' after 40 ns;
    
    UUT: twister Port Map  (data_in_1 => data_in_1,
                            data_in_2 => data_in_2,
                            data_out_1 => data_out_1,
                            data_out_2 => data_out_2,
                            rst => rst,
                            clk => clk);        
    
    process(clk)
    variable count : integer := 0;
    begin
        if rising_edge(clk) then
            if rst = '1' then
                count := 0;
                data_in_1 <= (others => '0');
                data_in_2 <= (others => '0');
            else
                data_in_1 <= std_logic_vector(to_unsigned(count,DATA_WIDTH));
                if count >= DELAY then
                    data_in_2 <= std_logic_vector(to_unsigned(count -DELAY,DATA_WIDTH));
                end if;
                count := count + 1;
            end if;
        end if;
    end process;

end Behavioral;
