----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.09.2017 17:28:47
-- Design Name: 
-- Module Name: FFT_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use ieee.math_real.all;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


entity FFT_tb is
--  Port ( );
end FFT_tb;
architecture behavioral of FFT_tb is

constant DATA_WIDTH : integer := 23;

component FFT is
--    Generic(DATA_WIDTH: integer := DATA_WIDTH;      --��� ��� �� ����� ���������� ����� �� 1 ��� ������, � ������� ��� 24
--            LENGTH : integer := 32;
--            COS_VALUE_WIDTH: integer := 17;
--            INIT_FILE : string := "G:\Temp_work\FFT\FFT.sim\sim_1\behav"
--            );
    Port ( ar : in std_logic_vector (DATA_WIDTH-1 downto 0);
           ai : in std_logic_vector (DATA_WIDTH-1 downto 0);
           br : in std_logic_vector (DATA_WIDTH-1 downto 0);
           bi : in std_logic_vector (DATA_WIDTH-1 downto 0);
           xr : out std_logic_vector (DATA_WIDTH-1 downto 0);
           xi : out std_logic_vector (DATA_WIDTH-1 downto 0);
           data_rdy : out std_logic;
--           yr : out std_logic_vector (DATA_WIDTH-1 downto 0);
--           yi : out std_logic_vector (DATA_WIDTH-1 downto 0);
--           zr : out std_logic_vector (DATA_WIDTH-1 downto 0);
--           zi : out std_logic_vector (DATA_WIDTH-1 downto 0);
           clk : in std_logic;
           rst : in std_logic);
end component FFT;

signal ar: std_logic_vector (DATA_WIDTH-1 downto 0);
signal ai: std_logic_vector (DATA_WIDTH-1 downto 0);
signal br: std_logic_vector (DATA_WIDTH-1 downto 0);
signal bi: std_logic_vector (DATA_WIDTH-1 downto 0);
signal xr: std_logic_vector (DATA_WIDTH-1 downto 0);
signal xi: std_logic_vector (DATA_WIDTH-1 downto 0);
signal data_rdy: std_logic;
signal amp : real;
--signal yr: std_logic_vector (DATA_WIDTH-1 downto 0);
--signal yi: std_logic_vector (DATA_WIDTH-1 downto 0);
--signal zr: std_logic_vector (DATA_WIDTH-1 downto 0);
--signal zi: std_logic_vector (DATA_WIDTH-1 downto 0);

signal clk: std_logic := '0';
signal rst: std_logic;
signal rst_reg: std_logic;
signal rst_reg_d: std_logic;

constant pow : natural := 5;
begin

clk <= not clk after 10 ns;
rst <= 'U','1' after 10 ns, '0' after 20 * 10 ns -5 ns;
rst_reg <= rst when rising_edge(clk);
rst_reg_d <= rst_reg when rising_edge(clk);
    UUT: FFT Port Map(ar => ar,
                        ai => ai,
                        br => br,
                        bi => bi,
                        xr => xr,
                        xi => xi,
                        clk => clk,
                        rst => rst_reg,
                        data_rdy => data_rdy
                        );
                        
    process(clk)
    variable temp1,temp2,temp3 : integer range 0 to 2**30;
    begin
        if rising_edge(clk) then
            if rst_reg_d = '1' then         --��� post-synth simulation  ���� ���������� ������ � ��������� �� ���� ���� ����� ������ ������
                ar <= std_logic_vector(to_signed(1*2**pow,DATA_WIDTH)); 
                br <= std_logic_vector(to_signed(1*2**pow,DATA_WIDTH)); 
                ai <= std_logic_vector(to_signed(1*2**pow,DATA_WIDTH));    
                bi <= std_logic_vector(to_signed(1*2**pow,DATA_WIDTH));    
                amp <= 0.0;
            else
                ar <= std_logic_vector(signed(ar) + to_signed(1*2**pow,DATA_WIDTH));
                br <= std_logic_vector(signed(br) + to_signed(1*2**pow,DATA_WIDTH));
                ai <= std_logic_vector(signed(ai) + to_signed(2*2**pow,DATA_WIDTH));
                bi <= std_logic_vector(signed(bi) + to_signed(2*2**pow,DATA_WIDTH));
                if data_rdy = '1' then
                    temp1 := to_integer(signed(xr)/(2**pow));
                    temp2 := to_integer(signed(xi)/(2**pow));
                    temp3 := temp1**2 + temp2**2;
                    amp <= (sqrt(real(temp3)));
                end if;
                
            end if;
        end if;
    end process;


end Behavioral;
