----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 25.09.2017 21:20:05
-- Design Name: 
-- Module Name: FFT - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.ram_pkg.all;

entity FFT is
    Generic(DATA_WIDTH: integer := 23;      --��� ��� �� ����� ���������� ����� �� 1 ��� ������, � ������� ��� 24
            LENGTH : integer := 16;
            COS_VALUE_WIDTH: integer := 17;
            INIT_FILE : string := "G:\Temp_work\FFT\FFT.sim\sim_1\behav"
            );
    Port ( ar : in std_logic_vector (DATA_WIDTH-1 downto 0);
           ai : in std_logic_vector (DATA_WIDTH-1 downto 0);
           br : in std_logic_vector (DATA_WIDTH-1 downto 0);
           bi : in std_logic_vector (DATA_WIDTH-1 downto 0);
           xr : out std_logic_vector (DATA_WIDTH-1 downto 0);
           xi : out std_logic_vector (DATA_WIDTH-1 downto 0);
           data_rdy : out std_logic;
--           yr : out std_logic_vector (DATA_WIDTH-1 downto 0);
--           yi : out std_logic_vector (DATA_WIDTH-1 downto 0);
--           zr : out std_logic_vector (DATA_WIDTH-1 downto 0);
--           zi : out std_logic_vector (DATA_WIDTH-1 downto 0);
           clk : in std_logic;
           rst : in std_logic);
end FFT;

architecture Behavioral of FFT is

function bit_reverse(slv1 : std_logic_vector) return std_logic_vector is
variable slv2 : std_logic_vector(slv1'range);
begin
    for i in slv1'range loop
        slv2(i) := slv1(slv1'length-1 - i);
    end loop;
    return slv2;
end function;

component FFT_block is
    Generic(DATA_WIDTH: integer := DATA_WIDTH;      --��� ��� �� ����� ���������� ����� �� 1 ��� ������, � ������� ��� 24
            DELAY: integer := 8;            -- ����� �������� � twister 
            COS_VALUE_WIDTH: integer := COS_VALUE_WIDTH;
            INIT_FILE : string := INIT_FILE
            );
    Port ( ar : in SIGNED (DATA_WIDTH-1 downto 0);
           ai : in SIGNED (DATA_WIDTH-1 downto 0);
           br : in SIGNED (DATA_WIDTH-1 downto 0);
           bi : in SIGNED (DATA_WIDTH-1 downto 0);
           yr : out SIGNED (DATA_WIDTH-1 downto 0);
           yi : out SIGNED (DATA_WIDTH-1 downto 0);
           zr : out SIGNED (DATA_WIDTH-1 downto 0);
           zi : out SIGNED (DATA_WIDTH-1 downto 0);
           clk : in std_logic;
           rst : in std_logic);
end component FFT_block;

signal sig_ar : signed (DATA_WIDTH-1 downto 0);
signal sig_ai : signed (DATA_WIDTH-1 downto 0);
signal sig_br : signed (DATA_WIDTH-1 downto 0);
signal sig_bi : signed (DATA_WIDTH-1 downto 0);
signal sig_yr : signed (DATA_WIDTH-1 downto 0);
signal sig_yi : signed (DATA_WIDTH-1 downto 0);
signal sig_zr : signed (DATA_WIDTH-1 downto 0);
signal sig_zi : signed (DATA_WIDTH-1 downto 0);
signal sig_yr1 : signed (DATA_WIDTH-1 downto 0);
signal sig_yi1 : signed (DATA_WIDTH-1 downto 0);
signal sig_zr1 : signed (DATA_WIDTH-1 downto 0);
signal sig_zi1 : signed (DATA_WIDTH-1 downto 0);
signal sig_yr2 : signed (DATA_WIDTH-1 downto 0);
signal sig_yi2 : signed (DATA_WIDTH-1 downto 0);
signal sig_zr2 : signed (DATA_WIDTH-1 downto 0);
signal sig_zi2 : signed (DATA_WIDTH-1 downto 0);

constant NUMBER_OF_BLOCKS : natural := clogb2(LENGTH);

type geom_progress_type is array(1 to NUMBER_OF_BLOCKS) of std_logic_vector(clogb2(LENGTH)-1 downto 0);

impure function geom_progress(n: integer) return integer is
variable ans : integer := 0;
begin
    for i in 1 to n loop
        ans := ans + LENGTH/(2**i);
    end loop;
    return ans;
end function;

function geom_progress_array_init(N:integer) return geom_progress_type is
variable progress_array : geom_progress_type;
begin
    for i in 1 to NUMBER_OF_BLOCKS loop
        progress_array(i) := std_logic_vector(to_unsigned(geom_progress(i),clogb2(LENGTH)));
    end loop;
    return progress_array;
end function; 
constant geom_progress_array : geom_progress_type := geom_progress_array_init(NUMBER_OF_BLOCKS);


type bus_array_type is array(0 to (NUMBER_OF_BLOCKS-1)*4+8-1) of signed(DATA_WIDTH-1 downto 0);
signal bus_array : bus_array_type;
constant FFT_BLOCK_DELAY : integer := 12;
signal del_line1 : std_logic_vector(2*FFT_BLOCK_DELAY-1 downto 0);
signal del_line2 : std_logic_vector(FFT_BLOCK_DELAY-1 downto 0);
signal rst_nd1 : std_logic;
signal rst_nd2 : std_logic;
signal rst_nd3 : std_logic;
signal rst_nd4 : std_logic;
signal rst_array : std_logic_vector(0 to clogb2(LENGTH)+3-1);
SIGNAL KOSTYL : integer range 0 to 1;    
    constant ADDR_BUS_WIDTH : integer := clogb2(LENGTH);    
    
    component xilinx_simple_dual_port_1_clock_ram is
    generic (
        RAM_WIDTH : integer := DATA_WIDTH;                      -- Specify RAM data width
        RAM_DEPTH : integer := LENGTH;                    -- Specify RAM depth (number of entries)
        RAM_PERFORMANCE : string := "LOW_LATENCY";      -- Select "HIGH_PERFORMANCE" or "LOW_LATENCY" 
        INIT_FILE : string := ""--"RAM_INIT.dat"                        -- Specify name/location of RAM initialization file if using one (leave blank if not)
        );
    
    port (
            addra : in std_logic_vector((ADDR_BUS_WIDTH-1) downto 0);     -- Write address bus, width determined from RAM_DEPTH
            addrb : in std_logic_vector((ADDR_BUS_WIDTH-1) downto 0);     -- Read address bus, width determined from RAM_DEPTH
            dina  : in std_logic_vector(RAM_WIDTH-1 downto 0);          -- RAM input data
            clka  : in std_logic;                                     -- Clock
            wea   : in std_logic;                                     -- Write enable
            enb   : in std_logic;                                     -- RAM Enable, for additional power savings, disable port when not in use
            rstb  : in std_logic;                                     -- Output reset (does not affect memory contents)
            regceb: in std_logic;                                     -- Output register enable
            doutb : out std_logic_vector(RAM_WIDTH-1 downto 0)                 -- RAM output data
        );
    end component;
    
        signal addra_xr_1 :  std_logic_vector((ADDR_BUS_WIDTH-1) downto 0);     -- Write address bus, width determined from RAM_DEPTH
        signal addrb_xr_1 :  std_logic_vector((ADDR_BUS_WIDTH-1) downto 0);     -- Read address bus, width determined from RAM_DEPTH
        signal dina_xr_1  :  std_logic_vector(DATA_WIDTH-1 downto 0);          -- RAM input data
        
        signal wea_1   :  std_logic;                                     -- Write enable
        signal enb_xr_1   :  std_logic;                                     -- RAM Enable, for additional power savings, disable port when not in use
        signal rstb_xr_1  :  std_logic;                                     -- Output reset (does not affect memory contents)
        signal regceb_xr_1:  std_logic;                                     -- Output register enable
        signal doutb_xr_1 :  std_logic_vector(DATA_WIDTH-1 downto 0);                 -- RAM output data
        
        signal addra_xi_1 :  std_logic_vector((ADDR_BUS_WIDTH-1) downto 0);     -- Write address bus, width determined from RAM_DEPTH
        signal addrb_xi_1 :  std_logic_vector((ADDR_BUS_WIDTH-1) downto 0);     -- Read address bus, width determined from RAM_DEPTH
        signal dina_xi_1  :  std_logic_vector(DATA_WIDTH-1 downto 0);          -- RAM input data
        
        
        signal enb_xi_1   :  std_logic;                                     -- RAM Enable, for additional power savings, disable port when not in use
        signal rstb_xi_1  :  std_logic;                                     -- Output reset (does not affect memory contents)
        signal regceb_xi_1:  std_logic;                                     -- Output register enable
        signal doutb_xi_1 :  std_logic_vector(DATA_WIDTH-1 downto 0);                 -- RAM output data
        signal temp : integer;
        attribute keep: string;
        attribute keep of temp : signal is "true";
        
--        signal addra_xr_2 :  std_logic_vector((ADDR_BUS_WIDTH-1) downto 0);     -- Write address bus, width determined from RAM_DEPTH
--        signal addrb_xr_2 :  std_logic_vector((ADDR_BUS_WIDTH-1) downto 0);     -- Read address bus, width determined from RAM_DEPTH
--        signal dina_xr_2  :  std_logic_vector(DATA_WIDTH-1 downto 0);          -- RAM input data
        
--        signal wea_2   :  std_logic;                                     -- Write enable
--        signal enb_xr_2   :  std_logic;                                     -- RAM Enable, for additional power savings, disable port when not in use
--        signal rstb_xr_2  :  std_logic;                                     -- Output reset (does not affect memory contents)
--        signal regceb_xr_2:  std_logic;                                     -- Output register enable
--        signal doutb_xr_2 :  std_logic_vector(DATA_WIDTH-1 downto 0);                 -- RAM output data
        
--        signal addra_xi_2 :  std_logic_vector((ADDR_BUS_WIDTH-1) downto 0);     -- Write address bus, width determined from RAM_DEPTH
--        signal addrb_xi_2 :  std_logic_vector((ADDR_BUS_WIDTH-1) downto 0);     -- Read address bus, width determined from RAM_DEPTH
--        signal dina_xi_2  :  std_logic_vector(DATA_WIDTH-1 downto 0);          -- RAM input data
        
        
--        signal enb_xi_2   :  std_logic;                                     -- RAM Enable, for additional power savings, disable port when not in use
--        signal rstb_xi_2  :  std_logic;                                     -- Output reset (does not affect memory contents)
--        signal regceb_xi_2:  std_logic;                                     -- Output register enable
--        signal doutb_xi_2 :  std_logic_vector(DATA_WIDTH-1 downto 0);                 -- RAM output data

        
        --attribute ram_style : string;
        --attribute ram_style of fifo_ram: label is "distributed";          --�� ���������
        
--        type ram is array(0 to DEPTH-1) of std_logic_vector(DATA_WIDTH-1 to 0);
--        signal fifo_ram: ram := (others=>(others=>'U'));
        
        --attribute ram_style: string;
        --attribute ram_style of fifo_ram: signal is "block";
        
--        attribute keep: boolean;
--        attribute keep of fifo_ram: signal is true;
--        attribute keep1: boolean;
--        attribute keep1 of dina: signal is true;  
begin



    buff_ram_xr_1: xilinx_simple_dual_port_1_clock_ram Port Map( addra => addra_xr_1,
                                                            addrb => addrb_xr_1,
                                                            dina => dina_xr_1,
                                                            clka => clk,
                                                            wea => wea_1,
                                                            enb => '1',
                                                            rstb => '0',
                                                            regceb => '0',
                                                            doutb => doutb_xr_1);
                                                            
    buff_ram_xi_1: xilinx_simple_dual_port_1_clock_ram Port Map( addra => addra_xi_1,
                                                            addrb => addrb_xi_1,
                                                            dina => dina_xi_1,
                                                            clka => clk,
                                                            wea => wea_1,
                                                            enb => '1',
                                                            rstb => '0',
                                                            regceb => '0',
                                                            doutb => doutb_xi_1);
    
--    buff_ram_xr_2: xilinx_simple_dual_port_1_clock_ram Port Map( addra => addra_xr_2,
--                                                            addrb => addrb_xr_2,
--                                                            dina => dina_xr_2,
--                                                            clka => clk,
--                                                            wea => wea_2,
--                                                            enb => '1',
--                                                            rstb => '0',
--                                                            regceb => '0',
--                                                            doutb => doutb_xr_2);
                                                            
--    buff_ram_xi_2: xilinx_simple_dual_port_1_clock_ram Port Map( addra => addra_xi_2,
--                                                            addrb => addrb_xi_2,
--                                                            dina => dina_xi_2,
--                                                            clka => clk,
--                                                            wea => wea_2,
--                                                            enb => '1',
--                                                            rstb => '0',
--                                                            regceb => '0',
--                                                            doutb => doutb_xi_2);

kostyl1:if LENGTH >= 64 generate
   KOSTYL <= 1;
end generate;
kostyl2:if LENGTH < 64 generate
   KOSTYL <= 0;
end generate;


bus_array(0) <= signed(ar);
bus_array(1) <= signed(ai);
bus_array(2) <= signed(br);
bus_array(3) <= signed(bi);
--sig_ar <= signed(ar);
--sig_ai <= signed(ai);
--sig_br <= signed(br);
--sig_bi <= signed(bi);


--rst_delay_1: process(clk)
--begin
--    if rising_edge(clk) then
--        del_line1 <= del_line1(2*FFT_BLOCK_DELAY-2 downto 0) & rst;
--        rst_nd1 <= del_line1(FFT_BLOCK_DELAY-1);
--        rst_nd2 <= del_line1(2*FFT_BLOCK_DELAY-1);
--    end if;
--end process;

data_rdy <= rst_array(NUMBER_OF_BLOCKS+2);

 process(clk,rst)
    variable count : integer range 0 to  9*(NUMBER_OF_BLOCKS) + geom_progress(NUMBER_OF_BLOCKS) + LENGTH/2 + LENGTH/8 +1 := 0;
    variable active : std_logic := '1';

begin    
    rst_array(0) <= rst; -- rst ������� ���������� �������� �� ���
    if rst = '1' then
--        rst_nd1 <= '1';
--        rst_nd2 <= '1';
        for i in 1 to NUMBER_OF_BLOCKS+1 loop
            rst_array(i) <= '1';
        end loop;
        rst_array(NUMBER_OF_BLOCKS+2) <= '0';
        --rst_nd3 <= '1';
        --rst_nd4 <= '1';
        --data_rdy <= '0';
    elsif rising_edge(clk) and active = '1'  then
        
        --rst_array(0) <= '0';
        for i in 1 to NUMBER_OF_BLOCKS loop
            if count = 9*i + geom_progress(i) then
                rst_array(i) <= '0';
            end if;
        end loop;
        if count = 9*(NUMBER_OF_BLOCKS) + geom_progress(NUMBER_OF_BLOCKS) + LENGTH/2   then
            rst_array(NUMBER_OF_BLOCKS+1) <= '0';
        end if;
        if count = 9*(NUMBER_OF_BLOCKS) + geom_progress(NUMBER_OF_BLOCKS) + LENGTH/2 + LENGTH/8  then
            rst_array(NUMBER_OF_BLOCKS+2) <= '1';            
        end if;
        if count = 9*(NUMBER_OF_BLOCKS) + geom_progress(NUMBER_OF_BLOCKS) + LENGTH/2 + LENGTH/8+1 then
            active := '0';
        end if;
--        if count = 9+LENGTH/2 then
--            rst_array(1) <= '0';
--            --rst_nd1 <= '0';
--        elsif count = 9+LENGTH/2 + 9+LENGTH/4 then
--            rst_array(2) <= '0';
--            --rst_nd2 <= '0';
--        elsif count = 9+LENGTH/2 + 9+LENGTH/4 + 9+LENGTH/8 then
--            rst_array(3) <= '0';
--        elsif count = 9+LENGTH/2 + 9+LENGTH/4 + 9+LENGTH/8 + 9+LENGTH/16  then
--            rst_array(4) <= '0';
--        elsif count = 9+LENGTH/2 + 9+LENGTH/4 + 9+LENGTH/8 + 9+LENGTH/16 +5  then
--            rst_array(5) <= '0';
--        elsif count = 9+LENGTH/2 + 9+LENGTH/4 + 9+LENGTH/8 + 9+LENGTH/16 +6  then
--            rst_array(6) <= '1';
--            active := '0';
--        end if;
        temp <= count;
        count := count + 1;
    end if;
end process;

--FFT1: FFT_block Generic Map(DELAY => 4)
--                Port Map (ar => sig_ar,
--                                ai =>sig_ai,
--                                br => sig_br,
--                                bi => sig_bi,
--                                yr => sig_yr1,
--                                yi => sig_yi1,
--                                zr => sig_zr1,
--                                zi => sig_zi1,
--                                clk => clk,
--                                rst => rst
--                                );
--FFT2: FFT_block Generic Map(DELAY => 2)
--                Port Map (ar => sig_yr1,
--                                ai =>sig_yi1,
--                                br => sig_zr1,
--                                bi => sig_zi1,
--                                yr => sig_yr2,
--                                yi => sig_yi2,
--                                zr => sig_zr2,
--                                zi => sig_zi2,
--                                clk => clk,
--                                rst => rst_nd1
--                                );
--FFT3: FFT_block Generic Map(DELAY => 1)
--                Port Map (ar => sig_yr2,
--                                ai =>sig_yi2,
--                                br => sig_zr2,
--                                bi => sig_zi2,
--                                yr => sig_yr,
--                                yi => sig_yi,
--                                zr => sig_zr,
--                                zi => sig_zi,
--                                clk => clk,
--                                rst => rst_nd2
--                                );

--FFT1: FFT_block Generic Map(DELAY => LENGTH/(2))
--                Port Map (ar => bus_array(0),
--                                ai =>bus_array(1),
--                                br => bus_array(2),
--                                bi => bus_array(3),
--                                yr => bus_array(4),
--                                yi => bus_array(5),
--                                zr => bus_array(6),
--                                zi => bus_array(7),
--                                clk => clk,
--                                rst => rst
--                                );
FFTn: for i in 1 to NUMBER_OF_BLOCKS generate
   
FFTi: FFT_block Generic Map(DELAY => LENGTH/(2**i))
                Port Map (ar => bus_array(0 + 4*(i-1)),
                                ai =>bus_array(1 + 4*(i-1)),
                                br => bus_array(2 + 4*(i-1)),
                                bi => bus_array(3 + 4*(i-1)),
                                yr => bus_array(4 + 4*(i-1)),
                                yi => bus_array(5 + 4*(i-1)),
                                zr => bus_array(6 + 4*(i-1)),
                                zi => bus_array(7 + 4*(i-1)),
                                clk => clk,
                                rst => rst_array(i-1)
                                );
end generate;

--FFT1: FFT_block Generic Map(DELAY => 4)
--                Port Map (ar => bus_array(0),
--                                ai =>bus_array(1),
--                                br => bus_array(2),
--                                bi => bus_array(3),
--                                yr => bus_array(4),
--                                yi => bus_array(5),
--                                zr => bus_array(6),
--                                zi => bus_array(7),
--                                clk => clk,
--                                rst => rst--rst_array(0)
--                                );
--FFT2: FFT_block Generic Map(DELAY => 2)
--                Port Map (ar => bus_array(4),
--                                ai =>bus_array(5),
--                                br => bus_array(6),
--                                bi => bus_array(7),
--                                yr => bus_array(8),
--                                yi => bus_array(9),
--                                zr => bus_array(10),
--                                zi => bus_array(11),
--                                clk => clk,
--                                rst => rst_nd1--rst_array(1)
--                                );
--FFT3: FFT_block Generic Map(DELAY => 1)
--                Port Map (ar => bus_array(8),
--                                ai =>bus_array(9),
--                                br => bus_array(10),
--                                bi => bus_array(11),
--                                yr => bus_array(12),
--                                yi => bus_array(13),
--                                zr => bus_array(14),
--                                zi => bus_array(15),
--                                clk => clk,
--                                rst => rst_nd2--rst_array(2)
--                                );   
                                
merger: process(clk,rst_array(NUMBER_OF_BLOCKS))
variable count1 : integer range 0 to LENGTH-1;
variable count2 : integer range 0 to LENGTH-1;
variable count2_init : integer range 0 to LENGTH-1;
variable slv_count2 : std_logic_vector(clogb2(LENGTH)-1 downto 0);
variable twist : std_logic := 'U';
variable ram_sel : std_logic := 'U';
variable xr: std_logic_vector(DATA_WIDTH-1 downto 0);
variable xi: std_logic_vector(DATA_WIDTH-1 downto 0);
begin
    if rst_array(NUMBER_OF_BLOCKS) = '1' then
        xr := (others => 'U');
        xi := (others => 'U');
        twist := '0';
        ram_sel := '0';
        count1 := 0;
        count2 := 0;
    elsif rising_edge(clk) then

        if count1 = 0 then
            twist := '0';
            ram_sel := not ram_sel;
            count2 := 0;
        elsif count1 = LENGTH/2 then
            twist := '1';
            count2 := 1;
        end if;
        
        slv_count2 := std_logic_vector(to_unsigned(count2,clogb2(LENGTH)));   
        
        case twist is
            when '0' =>
--                xr := std_logic_vector(sig_yr);
--                xi := std_logic_vector(sig_yi);            
                xr := std_logic_vector(bus_array((NUMBER_OF_BLOCKS-1)*4+8-4));
                xi := std_logic_vector(bus_array((NUMBER_OF_BLOCKS-1)*4+8-3));  
            when '1' =>
                xr := std_logic_vector(bus_array((NUMBER_OF_BLOCKS-1)*4+8-2));
                xi := std_logic_vector(bus_array((NUMBER_OF_BLOCKS-1)*4+8-1));
            when others => 
                report "Error in merger"
                severity Error;
        end case;
        wea_1 <= '1';
        --wea_2 <= '0';
        addra_xr_1 <= bit_reverse(slv_count2);
        addra_xi_1 <= bit_reverse(slv_count2);
        dina_xr_1 <= xr;
        dina_xi_1 <= xi;
--        case ram_sel is
--            when '0' =>
--                wea_1 <= '1';
--                wea_2 <= '0';
--                addra_xr_1 <= bit_reverse(slv_count2);
--                addra_xi_1 <= bit_reverse(slv_count2);
--                dina_xr_1 <= xr;
--                dina_xi_1 <= xi;
--            when '1' =>
--                wea_1 <= '0';
--                wea_2 <= '1';
--                addra_xr_2 <= bit_reverse(slv_count2);
--                addra_xi_2 <= bit_reverse(slv_count2);
--                dina_xr_2 <= xr;
--                dina_xi_2 <= xi;
--            when others =>
--                report "Error in merger"
--                severity Error;
--        end case;
        
        count2 := count2 + 2;
        count1 := count1 + 1;
        
        if count1 = LENGTH then
            count1 := 0;
        end if;
        
    end if;
end process;




buf_out: process(clk,rst_array(NUMBER_OF_BLOCKS+1)) 
variable count : integer range 0 to LENGTH-1;
begin
    if rst_array(NUMBER_OF_BLOCKS+1) = '1' then
        count := 1;
        xr <= (others => '0');
        xi <= (others => '0');
        addrb_xr_1 <= (others => '0');
    elsif rising_edge(clk) then
        addrb_xr_1 <= std_logic_vector(to_unsigned(count,clogb2(LENGTH)));
        addrb_xi_1 <= std_logic_vector(to_unsigned(count,clogb2(LENGTH)));
        xr <= doutb_xr_1;
        xi <= doutb_xi_1;
        count := count + 1;
    end if;
end process;





                                
--yr <= std_logic_vector(sig_yr);
--yi <= std_logic_vector(sig_yi);
--zr <= std_logic_vector(sig_zr);
--zi <= std_logic_vector(sig_zi);

end Behavioral;
