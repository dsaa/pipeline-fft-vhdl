----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.09.2017 11:26:03
-- Design Name: 
-- Module Name: FFT_block - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
--the latency is 10 clk cycles

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity FFT_block is
    Generic(DATA_WIDTH: integer := 23;      --��� ��� �� ����� ���������� ����� �� 1 ��� ������, � ������� ��� 24
            DELAY: integer := 16;            -- ����� �������� � twister 
            COS_VALUE_WIDTH: integer := 17;
            INIT_FILE : string := "G:\Temp_work\Xilinx_Labs\FFT_c_dr_comp\FFT2\FFT.sim\sim_1\behav"
            );
    Port ( ar : in SIGNED (DATA_WIDTH-1 downto 0);
           ai : in SIGNED (DATA_WIDTH-1 downto 0);
           br : in SIGNED (DATA_WIDTH-1 downto 0);
           bi : in SIGNED (DATA_WIDTH-1 downto 0);
           yr : out SIGNED (DATA_WIDTH-1 downto 0);
           yi : out SIGNED (DATA_WIDTH-1 downto 0);
           zr : out SIGNED (DATA_WIDTH-1 downto 0);
           zi : out SIGNED (DATA_WIDTH-1 downto 0);
           clk : in std_logic;
           rst : in std_logic);
end FFT_block;

architecture Behavioral of FFT_block is

    component delay_line is
    Generic(DATA_WIDTH: integer := DATA_WIDTH;
            DELAY: integer := DELAY); --minimal is 4
    Port ( data_in: in signed(DATA_WIDTH-1 downto 0);
           data_out: out signed(DATA_WIDTH-1 downto 0);
           clk: in std_logic;
           rst: in std_logic);
    end component delay_line;
    
    component butterfly is
    Generic(DATA_WIDTH: integer := DATA_WIDTH;      --��� ��� �� ����� ���������� ����� �� 1 ��� ������, � ������� ��� 24
            DELAY: integer := DELAY;            -- ����� �������� � twister ����� ������ butterfly 
            COS_VALUE_WIDTH: integer := COS_VALUE_WIDTH;
            INIT_FILE : string := INIT_FILE
            );
    Port ( ar : in SIGNED (DATA_WIDTH-1 downto 0);
           ai : in SIGNED (DATA_WIDTH-1 downto 0);
           br : in SIGNED (DATA_WIDTH-1 downto 0);
           bi : in SIGNED (DATA_WIDTH-1 downto 0);
           yr : out SIGNED (DATA_WIDTH-1 downto 0);
           yi : out SIGNED (DATA_WIDTH-1 downto 0);
           zr : out SIGNED (DATA_WIDTH-1 downto 0);
           zi : out SIGNED (DATA_WIDTH-1 downto 0);
           clk : in std_logic;
           rst : in std_logic);
    end component butterfly;
    
    
    component twister is
        Generic (DATA_WIDTH : integer := DATA_WIDTH;
                DELAY : integer := DELAY);
        Port ( data_in_1 : in SIGNED (DATA_WIDTH-1 downto 0);
               data_in_2 : in SIGNED (DATA_WIDTH-1 downto 0);
               data_out_1 : out SIGNED (DATA_WIDTH-1 downto 0);
               data_out_2 : out SIGNED (DATA_WIDTH-1 downto 0);
               clk : in STD_LOGIC;
               rst : in STD_LOGIC
               );
    end component twister;
        
    signal ar_nd : SIGNED (DATA_WIDTH-1 downto 0);
    signal ai_nd : SIGNED (DATA_WIDTH-1 downto 0);
    signal br_nd : SIGNED (DATA_WIDTH-1 downto 0);
    signal bi_nd : SIGNED (DATA_WIDTH-1 downto 0);
    signal ar_tw : SIGNED (DATA_WIDTH-1 downto 0);
    signal ai_tw : SIGNED (DATA_WIDTH-1 downto 0);
    signal br_tw : SIGNED (DATA_WIDTH-1 downto 0);
    signal bi_tw : SIGNED (DATA_WIDTH-1 downto 0);
    signal ar_tw_nd : SIGNED (DATA_WIDTH-1 downto 0);
    signal ai_tw_nd : SIGNED (DATA_WIDTH-1 downto 0);
    signal br_tw_nd : SIGNED (DATA_WIDTH-1 downto 0);
    signal bi_tw_nd : SIGNED (DATA_WIDTH-1 downto 0);
    signal del_line: std_logic_vector(DELAY-1-1 downto 0) := (others => '0');
    signal rst_nd : std_logic;
    signal rst_reg : std_logic;

begin
    
    ar_nd <= ar;
    ai_nd <= ai;
    br_delay: delay_line Port Map( data_in => br,
                                    data_out => br_nd,
                                    clk => clk,
                                    rst => rst
                                    );
    bi_delay: delay_line Port Map( data_in => bi,
                                    data_out => bi_nd,
                                    clk => clk,
                                    rst => rst
                                    );
    
    ar_br_twist: twister Port Map(data_in_1 => ar_nd,
                                    data_in_2 => br_nd,
                                    data_out_1 => ar_tw,
                                    data_out_2 => br_tw,
                                    clk => clk,
                                    rst => rst                                    
                                    );
    ai_bi_twist: twister Port Map(data_in_1 => ai_nd,
                                    data_in_2 => bi_nd,
                                    data_out_1 => ai_tw,
                                    data_out_2 => bi_tw,
                                    clk => clk,
                                    rst => rst                                   
                                    );
                                    
    
    ar_tw_delay: delay_line Port Map( data_in => ar_tw,
                                    data_out => ar_tw_nd,
                                    clk => clk,
                                    rst => rst
                                    );
    ai_tw_delay: delay_line Port Map( data_in => ai_tw,
                                    data_out => ai_tw_nd,
                                    clk => clk,
                                    rst => rst
                                    );

    --���� ��������� rst ��� butterfly �� ~8 ������
    process(clk,rst)
    variable count : integer range 0 to DELAY := 0;
    begin
        if rst = '1' then
            rst_nd <= '1';
            rst_reg <= '1';
            count := 0;
        elsif rising_edge(clk) then
            rst_reg <= '0';
            if count = DELAY-1 then
                rst_nd <= '0';
            else
                count := count + 1;
            end if;
        end if;
    end process;
    
--    rst_delay: if DELAY >= 3 generate    
--    process(clk)                         
--    begin
--        if rising_edge(clk) then
--            del_line <= del_line(DELAY-2-1 downto 0) & rst; -- +1 ���� �������� �� ��������?
--            rst_nd <= del_line(DELAY-1-1);            
--        end if;
--    end process;    
--    end generate;

--    rst_delay_2: if DELAY = 2 generate    
--    process(clk)                         
--    begin
--        if rising_edge(clk) then
--            rst_d <=  rst; 
--            rst_nd <= rst_d;            
--        end if;
--    end process;    
--    end generate;
    
--    rst_delay_1: if DELAY = 1 generate    
--    process(clk)                         
--    begin
--        if rising_edge(clk) then
--            rst_nd <= rst;            
--        end if;
--    end process;    
--    end generate;
    
    br_tw_nd <= br_tw;
    bi_tw_nd <= bi_tw;
    
    butt: butterfly Port Map (ar => ar_tw_nd,
                                ai => ai_tw_nd,
                                br => br_tw_nd,
                                bi => bi_tw_nd,
                                yr => yr,
                                yi => yi,
                                zr => zr,
                                zi => zi,
                                clk => clk,
                                rst => rst_nd
                                );

end Behavioral;
