----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.09.2017 13:39:58
-- Design Name: 
-- Module Name: experiments2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.math_real.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity experiments2 is
    Generic();
    Port ( );
end experiments2;

architecture Behavioral of experiments2 is

component cos_values is
generic (
    COS_VALUE_WIDTH : integer := 64;                      -- Specify RAM data width
    LENGTH : integer := 512;                    -- Specify RAM depth (number of entries)
    RAM_PERFORMANCE : string := "LOW_LATENCY";      -- Select "HIGH_PERFORMANCE" or "LOW_LATENCY" 
    INIT_FILE : string := ""--file path or blank 
    );

port (
        addra : in std_logic_vector((clogb2(LENGTH)-1) downto 0);     -- Write address bus, width determined from RAM_DEPTH
        addrb : in std_logic_vector((clogb2(LENGTH)-1) downto 0);     -- Read address bus, width determined from RAM_DEPTH
        dina  : in std_logic_vector(COS_VALUE_WIDTH-1 downto 0);		  -- RAM input data
        clka  : in std_logic;                       			  -- Clock
        wea   : in std_logic;                       			  -- Write enable
        enb   : in std_logic;                       			  -- RAM Enable, for additional power savings, disable port when not in use
        rstb  : in std_logic;                       			  -- Output reset (does not affect memory contents)
        regceb: in std_logic;                       			  -- Output register enable
        doutb : out std_logic_vector(COS_VALUE_WIDTH-1 downto 0)   			  -- RAM output data
    );

end component cos_values;
    
--    function cos_val(length: in integer; width: in integer; m: in integer) return signed is
--        variable y: signed(width-1 downto 0) := (others => '0');
--        variable N: integer := length;
--    begin
--        if m < N/4 then
--            y := signed(rom(m));
--        elsif m >= N/4 and m < N/2 then
--            y := -signed(rom(N/2-1-m));
--        elsif m >= N/2 and m < 3*N/4 then
--            y := -signed(rom(m-N/2));
--        elsif m >= 3*N/4 and m < N then
--            y := signed(rom(N-1-m));
--        else
--            report("Error")
--            severity ERROR;
--        end if;
--    end function;
    
--    function sin_val(length: in integer; width: in integer; m: in integer) return signed is
--        variable y: signed(width-1 downto 0) := (others => '0');
--        variable N: integer := length;
--    begin
--        if m < N/4 then
--            y := signed(rom(N/4-1-m));
--        elsif m >= N/4 and m < N/2 then
--            y := signed(rom(m - N/4));
--        elsif m >= N/2 and m < 3*N/4 then
--            y := -signed(rom(3*N/4-1 - m));
--        elsif m >= 3*N/4 and m < N then
--            y := -signed(rom(m - 3*N/4));
--        else
--            report("Error")
--            severity ERROR;
--        end if;
--    end function;
    
begin
    

end Behavioral;
