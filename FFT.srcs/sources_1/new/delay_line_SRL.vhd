----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 31.03.2017 19:10:41
-- Design Name: 
-- Module Name: delay_line - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--how is minimal delay? less than 2?

entity delay_line_SRL is
    Generic (constant DELAY : natural := 32;
            constant DATA_WIDTH: natural := 32); 
    Port (data_in :     in      std_logic_vector(DATA_WIDTH-1 downto 0);
          data_out:     out     std_logic_vector(DATA_WIDTH-1 downto 0);
          clk :         in      std_logic;
          rst :         in      std_logic);
end delay_line_SRL;

architecture Behavioral of delay_line_SRL is
    constant DELAY_SRL : integer := DELAY -1; --the registers give the latency on a one clock cycle
    type delay_ram is array(DATA_WIDTH-1 downto 0) of std_logic_vector(DELAY_SRL-1 downto 0);
    signal shift_reg : delay_ram := (others => (others=>'0'));
    
    
begin
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                data_out <= (others => '0');
                shift_reg <= (others => (others=>'0')); --требует дополнительной логики?
            else
                for i in DATA_WIDTH-1 downto 0 loop
                    shift_reg(i) <= shift_reg(i)(DELAY_SRL-2 downto 0) & data_in(i);
                end loop;
                for i in DATA_WIDTH-1 downto 0 loop
                        data_out(i) <= shift_reg(i)(DELAY_SRL-1);
                end loop;
            end if;
        end if;
    end process;
    


end Behavioral;
