----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.04.2017 19:49:07
-- Design Name: 
-- Module Name: delay_line_FIFO - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


--����������� �������� DELAY = 4


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity delay_line is
    Generic(DATA_WIDTH: integer := 32;
            DELAY: integer := 8); --minimal for FIFO is 4
    Port (  data_in: in signed(DATA_WIDTH-1 downto 0);
           data_out: out signed(DATA_WIDTH-1 downto 0);
           clk: in std_logic;
           rst: in std_logic);

end delay_line;

architecture Behavioral of delay_line is
    
    component FIFO is
    Generic(DATA_WIDTH: integer := DATA_WIDTH;
            DEPTH: integer := DELAY+1);
    Port (  data_in: in std_logic_vector(DATA_WIDTH-1 downto 0);
            data_out: out std_logic_vector(DATA_WIDTH-1 downto 0);
            clk: in std_logic;
            rst: in std_logic;
            rstout:out std_logic;
            wen: in std_logic;
            ren: in std_logic;
            is_full: out std_logic;
            is_empty: out std_logic);
    end component;
    
    
    signal rstout: std_logic;
    signal wen: std_logic;
    signal ren: std_logic;
    signal slv_data_in: std_logic_vector(DATA_WIDTH-1 downto 0);
    signal slv_data_out: std_logic_vector(DATA_WIDTH-1 downto 0);    
    signal count : integer range 0 to DELAY := 0;
    
    type delay_ram is array(DATA_WIDTH-1 downto 0) of std_logic_vector(DELAY-1-1 downto 0); -- �������� ���� �������� +1 ���� => DELAY-1
    signal shift_reg : delay_ram := (others => (others=>'0'));
    
    
begin
    
    slv_data_in <= std_logic_vector(data_in);
    data_out <= signed(slv_data_out);
    
    FIFO_RAM: if DELAY > 16 generate    

        delay_fifo: FIFO
        Port map (  data_in => slv_data_in,
                data_out => slv_data_out,
                clk => clk,
                rst => rst,
                rstout => open,
                wen => wen,
                ren => ren,
                is_full => open,
                is_empty => open);
    
    
        process(clk)
        --variable count : integer range 0 to DELAY := 0;       --������ ������ �� ���������� � ������?
        begin
            if rising_edge(clk) then
                if rst = '1' then
                    count <= 0;
                    wen <= '0';
                    ren <= '0';
                else
                    wen <= '1';                
                    if count = DELAY -4 then    --���������� �������� �� 3 ����� � ��������� FIFO, ������, ����� ������ ������ �� ���������� ������ �� 3 ����� ������
                                                --� ����� ���� ������� ���������� ������ ����������? �� ��� ��� ������?
                        ren <= '1';
                    else
                        count <= count + 1;
                    end if;
                end if;
            end if;
        end process;
    
    end generate;
    
    FIFO_SLR: if DELAY <= 16 and DELAY > 2 generate
        process(clk)
        
        begin
           if rising_edge(clk) then
               if rst = '1' then
                   slv_data_out <= (others => '0');
                   shift_reg <= (others => (others=>'0')); --������� �������������� ������?
               else
                   for i in DATA_WIDTH-1 downto 0 loop
                       shift_reg(i) <= shift_reg(i)(DELAY-1-2 downto 0) & slv_data_in(i);
                   end loop;
                   for i in DATA_WIDTH-1 downto 0 loop
                           slv_data_out(i) <= shift_reg(i)(DELAY-1-1);
                   end loop;
               end if;
           end if;
        end process;
    end generate;
    
    reg_delay_2: if DELAY = 2 generate
            process(clk)
            variable slv_data_in_d : std_logic_vector(DATA_WIDTH - 1 downto 0);            
            begin
               if rising_edge(clk) then
                   if rst = '1' then
                       slv_data_out <= (others => '0');
                       shift_reg <= (others => (others=>'0')); --������� �������������� ������?
                   else                                      
                       slv_data_out <= slv_data_in_d;  
                       slv_data_in_d :=  slv_data_in;                     
                   end if;
               end if;
            end process;
    end generate;
    
    reg_delay_1: if DELAY = 1 generate
        process(clk)        
        begin
           if rising_edge(clk) then
               if rst = '1' then
                   slv_data_out <= (others => '0');
                   shift_reg <= (others => (others=>'0')); --������� �������������� ������?
               else                                      
                   slv_data_out <= slv_data_in;                     
               end if;
           end if;
        end process;
    end generate;


end Behavioral;
