----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07.04.2017 11:39:04
-- Design Name: 
-- Module Name: twister - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity twister is
    Generic (DATA_WIDTH : integer := 32;
            DELAY : integer := 32);
    Port ( data_in_1 : in SIGNED (DATA_WIDTH-1 downto 0);
           data_in_2 : in SIGNED (DATA_WIDTH-1 downto 0);
           data_out_1 : out SIGNED (DATA_WIDTH-1 downto 0);
           data_out_2 : out SIGNED (DATA_WIDTH-1 downto 0);
           clk : in STD_LOGIC;
           rst : in STD_LOGIC
           );
end twister;

architecture Behavioral of twister is
    signal twist : std_logic := 'U';
    
begin
    
    
    data_out_1 <= data_in_1 when twist = '0' else
                    data_in_2 when twist = '1';
    data_out_2 <= data_in_2 when twist = '0' else
                        data_in_1 when twist = '1';

    process(clk)
    variable count : integer range 0 to DELAY-1:= 0;
    begin
        if rising_edge(clk) then
            if rst = '1' then
                count := 0;
                twist <= '0';
            else
                if count = DELAY-1 then
                    twist <= not twist;
                    count := 0;
                else
                    count := count + 1;
                end if;
            end if;
        end if;
    end process;

end Behavioral;
