
--  Xilinx Simple Dual Port Single Clock RAM
--  This code implements a parameterizable SDP single clock memory.
--  If a reset or enable is not necessary, it may be tied off or removed from the code.

library ieee;
use ieee.std_logic_1164.all;
--use ieee.math_real.all;
use ieee.numeric_std.all;

package cos_ram_pkg is
    function clogb2 (depth: in natural) return integer; -- depth should be the power of 2 else the depth would be rounded
end cos_ram_pkg;

package body cos_ram_pkg is

function clogb2( depth : natural) return integer is
variable temp    : integer := depth;
variable ret_val : integer := 0;
begin
    while temp > 1 loop
        ret_val := ret_val + 1;
        temp    := temp / 2;
    end loop;
return ret_val;
end function;

end package body cos_ram_pkg;

library ieee;
library work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ram_pkg.all;
USE std.textio.all;

entity cos_values is
generic (
    COS_VALUE_WIDTH : integer := 17;                      -- Specify RAM data width
    LENGTH : integer := 16;                    -- Specify RAM depth (number of entries)
    RAM_PERFORMANCE : string := "LOW_LATENCY";      -- Select "HIGH_PERFORMANCE" or "LOW_LATENCY" 
    INIT_FILE : string := "G:\Temp_work\FFT\FFT.sim\sim_1\behav"; --file path or blank 
    FILE_NAME_PATTERN: string := "\cos_values_length_"
    );

port (
        addra : in std_logic_vector((clogb2(LENGTH/4)+1-1) downto 0);     -- a number of values is LENGTH/4 + 1 therefore one more bit is required
        addrb : in std_logic_vector((clogb2(LENGTH/4)+1-1) downto 0);     -- Read address bus, width determined from RAM_DEPTH
        --dina  : in std_logic_vector(COS_VALUE_WIDTH-1 downto 0);		  -- RAM input data
        --dinb  : in std_logic_vector(COS_VALUE_WIDTH-1 downto 0);		  -- RAM input data
        clka  : in std_logic;                       			  -- Clock
        --wea   : in std_logic;                       			  -- Write enable
        --web   : in std_logic;                       			  -- Write enable
        ena   : in std_logic;                       			  -- RAM Enable, for additional power savings, disable port when not in use
        enb   : in std_logic;                       			  -- RAM Enable, for additional power savings, disable port when not in use
        rsta  : in std_logic;                       			  -- Output reset (does not affect memory contents)
        rstb  : in std_logic;                       			  -- Output reset (does not affect memory contents)
        regcea: in std_logic;                       			  -- Output register enable
        regceb: in std_logic;                       			  -- Output register enable
        douta : out std_logic_vector(COS_VALUE_WIDTH-1 downto 0);   	      -- RAM output data
        doutb : out std_logic_vector(COS_VALUE_WIDTH-1 downto 0)   			  -- RAM output data
    );

end cos_values;

architecture rtl of cos_values is

constant C_RAM_WIDTH : integer := COS_VALUE_WIDTH;
constant C_RAM_DEPTH : integer := LENGTH/4+1;
constant C_RAM_PERFORMANCE : string := RAM_PERFORMANCE;
constant C_INIT_FILE : string := INIT_FILE;

signal douta_reg : std_logic_vector(C_RAM_WIDTH-1 downto 0) := (others => '0');
signal doutb_reg : std_logic_vector(C_RAM_WIDTH-1 downto 0) := (others => '0');
type ram_type is array ( 0 to C_RAM_DEPTH-1) of std_logic_vector (C_RAM_WIDTH-1 downto 0);          -- 2D Array Declaration for RAM signal
signal ram_data_a : std_logic_vector(C_RAM_WIDTH-1 downto 0) ;
signal ram_data_b : std_logic_vector(C_RAM_WIDTH-1 downto 0) ;

-- The folowing code either initializes the memory values to a specified file or to all zeros to match hardware

impure function initramfromfile (ramfilename : in string) return ram_type is
file ramfile	: text is in ramfilename;
variable ramfileline : line;
variable ram_name	: ram_type;
variable bitvec : bit_vector(C_RAM_WIDTH-1 downto 0);

begin
    for i in ram_type'range loop
        readline (ramfile, ramfileline);
        read (ramfileline, bitvec);
        ram_name(i) := to_stdlogicvector(bitvec);--to_stdlogicvector(bitvec);
    end loop;
    return ram_name;
end function;

--function init(constant length: in integer; constant width: in integer) return ram_type is
--        variable y: ram_type := (others => (others => '0'));
--        variable x: real := 0.0;    
--        variable k: integer := 0;
--        constant N: integer := length/4; --����� ������ �������� ������� �������� ��� ��������������� ���� �������� sin � cos
--        constant pi: real := 3.14;--math_pi; --pi*2**29
--        variable temp1: integer;
--        --variable temp2: integer;    --������?
--    begin
--        y(0) := (width-1 => '0', others => '1'); --cos(0) = 0.9999999
--        for k in 1 to N-1 loop                                    
--            x := 2.0*pi/real(length)*real(k);            
--            temp1 := integer(round(cos(x)*2.0**(width-1)));--���� ��� ��������� �� ����, ������� width-1  --(integer(floor(1.0*real(2**(width+8)))) - integer(floor(x**2/4.0*real(2**(width+8)))) + integer(floor(x**4/24.0*real(2**(width+8)))) - integer(floor(x**6/720.0*real(2**(width+8)))) + integer(floor(x**8/40320.0*real(2**(width+8)))));           
--            y(k) := std_logic_vector(to_signed(temp1,width));
--        end loop;
--    return y;
--    end function;


impure function init_from_file_or_zero(ramfile : string) return ram_type is
variable l: line;
begin
    if ramfile'length /= 0 then        
        return InitRamFromFile(ramfile & FILE_NAME_PATTERN & integer'image(LENGTH) & ".txt");
    else
        return (others => (others => '1'));
    end if;
end;

signal ram_name : ram_type := init_from_file_or_zero(C_INIT_FILE);
attribute ram_style : string;
attribute ram_style of ram_name: signal is "block";

begin

process(clka)
begin
    if(clka'event and clka = '1') then
        if(ena = '1') then
--            if(wea = '1') then
--              ram_name(to_integer(unsigned(addra))) <= dina;
--            end if;
        ram_data_a <= ram_name(to_integer(unsigned(addra)));
    end if;
    end if;
end process;

process(clka)
begin
    if(clka'event and clka = '1') then
        if(enb = '1') then
--            if(web = '1') then
--              ram_name(to_integer(unsigned(addra))) <= dinb;
--            end if;
        ram_data_b <= ram_name(to_integer(unsigned(addrb)));
    end if;
    end if;
end process;

--  Following code generates LOW_LATENCY (no output register)
--  Following is a 1 clock cycle read latency at the cost of a longer clock-to-out timing

no_output_register : if C_RAM_PERFORMANCE = "LOW_LATENCY" generate
    douta <= ram_data_a;
    doutb <= ram_data_b;
end generate;

--  Following code generates HIGH_PERFORMANCE (use output register)
--  Following is a 2 clock cycle read latency with improved clock-to-out timing

output_register_A : if C_RAM_PERFORMANCE = "HIGH_PERFORMANCE"  generate
process(clka)   --port A
begin
    if(clka'event and clka = '1') then
        if(rsta = '1') then
            douta_reg <= (others => '0');
        elsif(regcea = '1') then
            douta_reg <= ram_data_a;
        end if;
    end if;
end process;
douta <= douta_reg;
end generate;

output_register_B : if C_RAM_PERFORMANCE = "HIGH_PERFORMANCE"  generate
process(clka)   --port B
begin
    if(clka'event and clka = '1') then
        if(rstb = '1') then
            doutb_reg <= (others => '0');
        elsif(regceb = '1') then
            doutb_reg <= ram_data_b;
        end if;
    end if;
end process;

doutb <= doutb_reg;
end generate;

end rtl;

		
						