----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.04.2017 16:14:03
-- Design Name: 
-- Module Name: FIFO - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

--�������� -  ����� ���������� �� 3-� ���� � ������� ��������� ������� ren


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;



entity FIFO is
    Generic(DATA_WIDTH: integer := 32;
            DEPTH: integer := 128);
    Port (  data_in: in std_logic_vector(DATA_WIDTH-1 downto 0);
            data_out: out std_logic_vector(DATA_WIDTH-1 downto 0);
            clk: in std_logic;
            rst: in std_logic;
            rstout:out std_logic;
            wen: in std_logic;
            ren: in std_logic;
            is_full: out std_logic;
            is_empty: out std_logic);
end FIFO;

architecture Behavioral of FIFO is

    function clogb2( depth : natural) return integer is
    variable temp    : integer := depth;
    variable ret_val : integer := 0;
    begin
        while temp > 1 loop
            ret_val := ret_val + 1;
            temp    := temp / 2;
        end loop;
    return ret_val;
    end function;
    
    constant ADDR_BUS_WIDTH : integer := clogb2(DEPTH);
    
    
    component xilinx_simple_dual_port_1_clock_ram is
    generic (
        RAM_WIDTH : integer := DATA_WIDTH;                      -- Specify RAM data width
        RAM_DEPTH : integer := DEPTH;                    -- Specify RAM depth (number of entries)
        RAM_PERFORMANCE : string := "LOW_LATENCY";      -- Select "HIGH_PERFORMANCE" or "LOW_LATENCY" 
        INIT_FILE : string := ""--"RAM_INIT.dat"                        -- Specify name/location of RAM initialization file if using one (leave blank if not)
        );
    
    port (
            addra : in std_logic_vector((ADDR_BUS_WIDTH-1) downto 0);     -- Write address bus, width determined from RAM_DEPTH
            addrb : in std_logic_vector((ADDR_BUS_WIDTH-1) downto 0);     -- Read address bus, width determined from RAM_DEPTH
            dina  : in std_logic_vector(RAM_WIDTH-1 downto 0);          -- RAM input data
            clka  : in std_logic;                                     -- Clock
            wea   : in std_logic;                                     -- Write enable
            enb   : in std_logic;                                     -- RAM Enable, for additional power savings, disable port when not in use
            rstb  : in std_logic;                                     -- Output reset (does not affect memory contents)
            regceb: in std_logic;                                     -- Output register enable
            doutb : out std_logic_vector(RAM_WIDTH-1 downto 0)                 -- RAM output data
        );
    end component;
    
        signal addra :  std_logic_vector((ADDR_BUS_WIDTH-1) downto 0);     -- Write address bus, width determined from RAM_DEPTH
        signal addrb :  std_logic_vector((ADDR_BUS_WIDTH-1) downto 0);     -- Read address bus, width determined from RAM_DEPTH
        signal dina  :  std_logic_vector(DATA_WIDTH-1 downto 0);          -- RAM input data
        
        signal wea   :  std_logic;                                     -- Write enable
        signal enb   :  std_logic;                                     -- RAM Enable, for additional power savings, disable port when not in use
        signal rstb  :  std_logic;                                     -- Output reset (does not affect memory contents)
        signal regceb:  std_logic;                                     -- Output register enable
        signal doutb :  std_logic_vector(DATA_WIDTH-1 downto 0);                 -- RAM output data
        
        --attribute ram_style : string;
        --attribute ram_style of fifo_ram: label is "distributed";          --�� ���������
        
--        type ram is array(0 to DEPTH-1) of std_logic_vector(DATA_WIDTH-1 to 0);
--        signal fifo_ram: ram := (others=>(others=>'U'));
        
        --attribute ram_style: string;
        --attribute ram_style of fifo_ram: signal is "block";
        
--        attribute keep: boolean;
--        attribute keep of fifo_ram: signal is true;
--        attribute keep1: boolean;
--        attribute keep1 of dina: signal is true;  
begin

    fifo_ram: xilinx_simple_dual_port_1_clock_ram Port Map( addra => addra,
                                                            addrb => addrb,
                                                            dina => dina,
                                                            clka => clk,
                                                            wea => wea,
                                                            enb => enb,
                                                            rstb => rstb,
                                                            regceb => regceb,
                                                            doutb => doutb);
    

    
    process(clk)
    variable first: integer range 0 to DEPTH-1 := 0;
    variable last: integer range 0 to DEPTH-1 := 0;
    begin
        if rising_edge(clk) then
            if rst = '1' or rstb = '1' then             --��� �� ����� � ����� ������? ��� ����� �� ��� ����������� ������, � ��� ��������� �������� ����� ������ B
                data_out <= (others => '0');
                rstout <= '1';
                first := 0;
                last := 0;
                is_empty <= '0';
                is_full <= '0';
            else
                rstout <= '0';
                wea <= '0';
                enb <= '0';
                if wen = '1' then
                    wea <= '1';
                    addra <= std_logic_vector(to_unsigned(first,ADDR_BUS_WIDTH));
                    dina <= data_in;
                    first := first + 1;
                    if first = last then
                        is_full <= '1';
                    else
                        is_full <= '0';
                    end if;
                end if;
                if ren = '1' then
                    enb <= '1';
                    addrb <= std_logic_vector(to_unsigned(last,ADDR_BUS_WIDTH));                 
                    last := last + 1;
                    if first = last then
                        is_empty <= '1';
                    else
                        is_empty <= '0';
                    end if;
                end if;
                if first = DEPTH then
                    first := 0;
                end if;
                if last = DEPTH then
                    last := 0;
                end if;
                data_out <= doutb;
            end if;
        end if;
    end process;
    
    
    
    
    
    
    
    
    
end Behavioral;












