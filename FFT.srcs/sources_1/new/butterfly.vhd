----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Dolmatov A.A.
-- 
-- Create Date: 07.04.2017 21:16:57
-- Design Name: FFT
-- Module Name: butterfly - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;

use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.ram_pkg.all;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity butterfly is
    Generic(DATA_WIDTH: integer := 23;      --��� ��� �� ����� ���������� ����� �� 1 ��� ������, � ������� ��� 24
            DELAY: integer := 2;            -- ����� �������� � twister ����� ������ butterfly 
            COS_VALUE_WIDTH: integer := 17;
            INIT_FILE : string := "G:\Temp_work\FFT\FFT.sim\sim_1\behav"
            );
    Port ( ar : in SIGNED (DATA_WIDTH-1 downto 0);
           ai : in SIGNED (DATA_WIDTH-1 downto 0);
           br : in SIGNED (DATA_WIDTH-1 downto 0);
           bi : in SIGNED (DATA_WIDTH-1 downto 0);
           yr : out SIGNED (DATA_WIDTH-1 downto 0);
           yi : out SIGNED (DATA_WIDTH-1 downto 0);
           zr : out SIGNED (DATA_WIDTH-1 downto 0);
           zi : out SIGNED (DATA_WIDTH-1 downto 0);
           clk : in std_logic;
           rst : in std_logic);
end butterfly;

architecture Behavioral of butterfly is


    signal ar_1d : SIGNED (DATA_WIDTH-1 downto 0);
    signal ai_1d : SIGNED (DATA_WIDTH-1 downto 0);
    signal br_1d : SIGNED (DATA_WIDTH-1 downto 0);
    signal bi_1d : SIGNED (DATA_WIDTH-1 downto 0);
    signal ar_2d : SIGNED (DATA_WIDTH-1 downto 0);
    signal ai_2d : SIGNED (DATA_WIDTH-1 downto 0);
    signal br_2d : SIGNED (DATA_WIDTH-1 downto 0);
    signal bi_2d : SIGNED (DATA_WIDTH-1 downto 0);
    signal ar_d : SIGNED (DATA_WIDTH-1 downto 0);
    signal ai_d : SIGNED (DATA_WIDTH-1 downto 0);
    signal br_d : SIGNED (DATA_WIDTH-1 downto 0);
    signal bi_d : SIGNED (DATA_WIDTH-1 downto 0);
    signal ar_dd : SIGNED (DATA_WIDTH-1+1 downto 0); -- after addition
    signal ai_dd : SIGNED (DATA_WIDTH-1+1 downto 0);
    signal br_dd : SIGNED (DATA_WIDTH-1+1 downto 0);
    signal bi_dd : SIGNED (DATA_WIDTH-1+1 downto 0);
    signal ar_nd  : SIGNED (DATA_WIDTH-1+1 downto 0);
    signal ai_nd  : SIGNED (DATA_WIDTH-1+1 downto 0);
    signal br_nd  : SIGNED (DATA_WIDTH+COS_VALUE_WIDTH-1+2 downto 0);   --after multiplication
    signal bi_nd  : SIGNED (DATA_WIDTH+COS_VALUE_WIDTH-1+2 downto 0);
    signal cos_value : signed (COS_VALUE_WIDTH-1 downto 0);
    signal sin_value : signed (COS_VALUE_WIDTH-1 downto 0);
    signal slv_ar_nd  : std_logic_vector (DATA_WIDTH-1+1 downto 0);
    signal slv_ai_nd  : std_logic_vector (DATA_WIDTH-1+1 downto 0);
    signal slv_ar_dd : std_logic_vector (DATA_WIDTH-1+1 downto 0);
    signal slv_ai_dd : std_logic_vector (DATA_WIDTH-1+1 downto 0);
    signal rst_d : std_logic := 'U';
    signal rst_dd: std_logic := 'U';
    signal rst_ddd: std_logic := 'U';
    signal rst_dddd: std_logic := 'U';
    signal rst_ddddd: std_logic := 'U';
    signal rst_dddddd: std_logic := 'U';
    signal addra : std_logic_vector(clogb2(DELAY*2/4)+1-1 downto 0);
    signal addrb : std_logic_vector(clogb2(DELAY*2/4)+1-1 downto 0);
    signal harm_value_a: std_logic_vector(COS_VALUE_WIDTH-1 downto 0);
    signal harm_value_b: std_logic_vector(COS_VALUE_WIDTH-1 downto 0);
    signal count, count_d, count_dd: integer range 0 to DELAY-1 := 0;
    
    component cmult is
      generic (AWIDTH : natural := DATA_WIDTH +1;                   -- +1 because of sums is multiplicated ==24 - optimum size of 1st input of multiplier
               BWIDTH : natural := COS_VALUE_WIDTH                  --17  -- size of 2nd input of multiplier
              );
      port (clk    : in  std_logic;
            ar : in  signed(AWIDTH-1 downto 0); -- 1st input's real part
            ai : in  signed(AWIDTH-1 downto 0); -- 1st input's imaginary part
            br : in  signed(BWIDTH-1 downto 0); -- 2nd input's real part
            bi : in  signed(BWIDTH-1 downto 0); -- 2nd input's imaginary part
            pr : out signed(AWIDTH+BWIDTH downto 0);  -- real part of output
            pi : out signed(AWIDTH+BWIDTH downto 0)); -- imaginary part of output
    end component;
    
    component delay_line is
        Generic(DATA_WIDTH: integer := DATA_WIDTH+1; -- +1 because of the sum is delayed
                DELAY: integer := 4);                -- the signal latency in cmult   
        Port ( data_in: in signed(DATA_WIDTH-1 downto 0); --����� DATA_WIDTH ������� � generic ��� ������� �������� � butterfly? ������� � generic
               data_out: out signed(DATA_WIDTH-1 downto 0);
               clk: in std_logic;
               rst: in std_logic);    
    end component delay_line;
    
    component delay_line_SRL is
        Generic ( DELAY : natural := 4;       
                  DATA_WIDTH: natural := DATA_WIDTH+1); 
        Port (data_in :     in      std_logic_vector(DATA_WIDTH-1 downto 0);
              data_out:     out     std_logic_vector(DATA_WIDTH-1 downto 0);
              clk :         in      std_logic;
              rst :         in      std_logic);
    end component delay_line_SRL;
    
    component cos_values is
    generic (
        COS_VALUE_WIDTH : integer := COS_VALUE_WIDTH;                      -- Specify RAM data width
        LENGTH : integer := 2*DELAY;                    -- Specify RAM depth (number of entries)
        RAM_PERFORMANCE : string := "LOW_LATENCY";      -- Select "HIGH_PERFORMANCE" or "LOW_LATENCY" 
        INIT_FILE : string := INIT_FILE;      --file name pattern or blank 
        FILE_NAME_PATTERN : string := "\cos_values_length_"
        );
    
    port (
            addra : in std_logic_vector((clogb2(LENGTH/4)+1-1) downto 0);     -- Write address bus, width determined from RAM_DEPTH
            addrb : in std_logic_vector((clogb2(LENGTH/4)+1-1) downto 0);     -- Read address bus, width determined from RAM_DEPTH
            clka  : in std_logic;                                     -- Clock
            ena   : in std_logic;                                     -- RAM Enable, for additional power savings, disable port when not in use
            enb   : in std_logic;                                     -- RAM Enable, for additional power savings, disable port when not in use
            rsta  : in std_logic;                                     -- Output reset (does not affect memory contents)
            rstb  : in std_logic;                                     -- Output reset (does not affect memory contents)
            regcea: in std_logic;                                     -- Output register enable
            regceb: in std_logic;                                     -- Output register enable
            douta : out std_logic_vector(COS_VALUE_WIDTH-1 downto 0);                 -- RAM output data
            doutb : out std_logic_vector(COS_VALUE_WIDTH-1 downto 0)                 -- RAM output data
        );
    
    end component cos_values;
    
    --type rom_type is array(0 to DELAY*2/4-1) of std_logic_vector(COS_VALUE_WIDTH-1 to 0); --����� �������� �� N/2 ������ � twister ���� ��������� �� exp(j2pi/N)    
   
    procedure get_cos_value(signal m: in integer;signal addra : out std_logic_vector)  is        
           constant N: integer := 2*DELAY; --���� ������ cos ����� LENGTH = DELAY*2          
       begin           
           if m < N/4+1 then
               addra <= std_logic_vector(to_unsigned(m, addra'length));           
           elsif m >= N/4+1 and m < N/2+1 then
               addra <= std_logic_vector(to_unsigned(N/2-m, addra'length));            
            elsif m >= N/2+1 and m < 3*N/4+1 then
               addra <= std_logic_vector(to_unsigned(m-N/2, addra'length));            
           elsif m >= 3*N/4+1 and m < N then
               addra <= std_logic_vector(to_unsigned(N-m, addra'length));                
           else
               report("get cos value error: m_d = "&integer'image(m))
               severity ERROR;
           end if;   
       end procedure;       
       
       procedure get_cos_sign(signal m_d: in integer; signal harm_value_a: in std_logic_vector; signal cos_value : out signed)  is        
       constant N: integer := 2*DELAY; --���� ������ cos ����� LENGTH = DELAY*2          
       begin           
           if m_d < N/4+1 then          
              cos_value <= (signed(harm_value_a));
           elsif m_d >= N/4+1 and m_d < N/2+1 then          
              cos_value <= -(signed(harm_value_a));   
           elsif m_d >= N/2+1 and m_d < 3*N/4+1 then           
              cos_value <= -(signed(harm_value_a)); 
           elsif m_d >= 3*N/4+1 and m_d < N then           
              cos_value <= (signed(harm_value_a));            
           else                
              report("get cos sign error: m_d = "&integer'image(m_d))
              severity ERROR;
           end if;
       end procedure;       
       
       procedure get_sin_value(signal m: in integer; signal addrb : out std_logic_vector)  is        
           constant N: integer := 2*DELAY; --���� ������ cos ����� LENGTH = DELAY*2
       begin
           if m < N/4+1 then
               addrb <= std_logic_vector(to_unsigned(N/4-m, addrb'length));           
           elsif m >= N/4+1 and m < N/2+1 then
               addrb <= std_logic_vector(to_unsigned(m - N/4, addrb'length));                  
           elsif m >= N/2+1 and m < 3*N/4+1 then
               addrb <= std_logic_vector(to_unsigned(3*N/4 - m, addrb'length));                      
           elsif m >= 3*N/4+1 and m < N then
               addrb <= std_logic_vector(to_unsigned(m - 3*N/4, addrb'length));                  
           else
               report("get sin value error")
               severity ERROR;
           end if;
       end procedure;   
   
       procedure get_sin_sign(signal m_d: in integer; signal harm_value_b: in std_logic_vector; signal sin_value : out signed)  is        
           constant N: integer := 2*DELAY; --���� ������ cos ����� LENGTH = DELAY*2
       begin
           if m_d < N/4+1 then            
               sin_value <= -signed(harm_value_b);  --����� �������������, ��� ��� ��������� �� ����� �����
           elsif m_d >= N/4+1 and m_d < N/2+1 then            
               sin_value <= -signed(harm_value_b);            
           elsif m_d >= N/2+1 and m_d < 3*N/4+1 then           
               sin_value <= signed(harm_value_b);            
           elsif m_d >= 3*N/4+1 and m_d < N then           
               sin_value <= signed(harm_value_b);            
           else
               report("get sin sign error")
               severity ERROR;
           end if;
       end procedure;       
    
begin    
    process(clk)
    begin
        if rising_edge(clk) then
            ar_1d <= signed(ar);
            ai_1d <= signed(ai);            --������ ���������� ����� ��������???
            br_1d <= signed(br);            --!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            bi_1d <= signed(bi);            --������ ��� butterfly_tb ��� �� ��������
--            ar_2d <= ar_1d;
--            ai_2d <= ai_1d;
--            br_2d <= br_1d;
--            bi_2d <= bi_1d;
            ar_d <= ar_1d;
            ai_d <= ai_1d;
            br_d <= br_1d;
            bi_d <= bi_1d;
--            ar_d <= signed(ar);
--            ai_d <= signed(ai);
--            br_d <= signed(br);
--            bi_d <= signed(bi);
            ar_dd <= resize(ar_d,DATA_WIDTH+1) + resize(br_d,DATA_WIDTH+1);
            ai_dd <= resize(ai_d,DATA_WIDTH+1) + resize(bi_d,DATA_WIDTH+1);
            br_dd <= resize(ar_d,DATA_WIDTH+1) - resize(br_d,DATA_WIDTH+1);
            bi_dd <= resize(ai_d,DATA_WIDTH+1) - resize(bi_d,DATA_WIDTH+1);         
            
        end if;
    end process;
    
    cos_val_rom: if DELAY > 1 generate
    cos_val_rom: cos_values Port Map (clka => clk,
                                        addra => addra,
                                        addrb => addrb,
                                        douta => harm_value_a,
                                        doutb => harm_value_b,
                                        regcea => '1',
                                        regceb => '1',
                                        rsta => '0',
                                        rstb => '0',
                                        ena => '1',
                                        enb => '1');  
    end generate;
                                        
                                        
        delay_line_ar: delay_line_SRL  Generic Map (DELAY => 6,                --����� �������� ������ cmult?
                                                    DATA_WIDTH => DATA_WIDTH+1) --this has been pointed in component declaration already therefore it is not necessary there
                                        Port Map (  data_in => slv_ar_dd,
                                                    data_out => slv_ar_nd,
                                                    clk => clk,
                                                    rst => '0');                --� �������, ��� �� ��������������, �� ������� ���� �� ���� ��������� �� ���������
        delay_line_ai: delay_line_SRL Generic Map (DELAY => 6,
                                                    DATA_WIDTH => DATA_WIDTH+1)
                                        Port Map (   data_in => slv_ai_dd,
                                                    data_out => slv_ai_nd,
                                                    clk => clk,
                                                    rst => '0');
        slv_ar_dd <= std_logic_vector(ar_dd);
        slv_ai_dd <= std_logic_vector(ai_dd);
        ar_nd <= signed(slv_ar_nd);
        ai_nd <= signed(slv_ai_nd);
        
        process(clk)
        begin
            if rising_edge(clk) then
                rst_d <= rst;
                rst_dd <= rst_d;
                rst_ddd <= rst_dd;
                rst_dddd <= rst_ddd;
                rst_ddddd <= rst_dddd;
                rst_dddddd <= rst_ddddd;
            end if;
        end process;
        
        counter:if DELAY > 1 generate
        process(clk) -- to count         
        begin
            if rising_edge(clk) then
                if rst = '1' then       --����� ����, ���� ������� ����� � ���������, ���� ������ �� ����� ������ �� cmult? ������ �������� ������������ � ������ � 2 �����                                          
                                          --���� ��������� ��� � ���� ������ -������ � harm_value �� ���� ������� �� �������� m � ���������
                                          --������� - ������� count �������� � �� ����������
                    count <= 0;
                    count_d <= 0;
                    count_dd <= 0;
                    cos_value <= (others => '0');
                    sin_value <= (others => '0');                    
                else
                    get_cos_value(count,addra);
                    get_cos_sign(count_dd,harm_value_a, cos_value);
                    get_sin_value(count,addrb);
                    get_sin_sign(count_dd,harm_value_b, sin_value);
                    if count /= DELAY-1 then
                        count <= count + 1;                        
                    else
                        count <= 0;
                    end if;
                    count_d <= count;
                    count_dd <= count_d;
                end if;    
            end if;
        end process;
        end generate;
        
        counter_del_1:if DELAY = 1 generate
        process(clk) -- to count         
        begin
            if rising_edge(clk) then
                if rst = '1' then       --����� ����, ���� ������� ����� � ���������, ���� ������ �� ����� ������ �� cmult? ������ �������� ������������ � ������ � 2 �����                                          
                                          --���� ��������� ��� � ���� ������ -������ � harm_value �� ���� ������� �� �������� m � ���������
                                          --������� - ������� count �������� � �� ����������
                    count <= 0;
                    count_d <= 0;
                    count_dd <= 0;
                    cos_value <= ('0',others => '1');
                    sin_value <= (others => '0');                    
                else
                    if count /= DELAY-1 then
                        count <= count + 1;                        
                    else
                        count <= 0;
                    end if;
                    count_d <= count;
                    count_dd <= count_d;
                end if;    
            end if;
        end process;
        end generate;
        
        complex_multiplier: cmult Port Map (clk => clk,                                            
                                            ar => br_dd,
                                            ai => bi_dd,
                                            br => cos_value,
                                            bi => sin_value,
                                            pr => br_nd,
                                            pi => bi_nd);
        
        yr <= signed(ar_nd(DATA_WIDTH-1 downto 0 ));
        yi <= signed(ai_nd(DATA_WIDTH-1 downto 0 ));
        zr <= signed(br_nd(DATA_WIDTH+COS_VALUE_WIDTH-1-1 downto COS_VALUE_WIDTH-1 )); -- ���� ��� �������� => COS_VALUE_WIDTH-1
        zi <= signed(bi_nd(DATA_WIDTH+COS_VALUE_WIDTH-1-1 downto COS_VALUE_WIDTH-1 ));
        
        
                                                 

end Behavioral;
